import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logging/logging.dart';

import 'app.dart';
import 'bootstrap.dart';
import 'core/entities/environment.dart';
import 'database.dart';
import 'logger.dart';

/// The main entry point for the application.
void main() async {
  // Make sure, everything is initialized before its used
  WidgetsFlutterBinding.ensureInitialized();

  // Some Default Configuration
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(statusBarColor: Colors.transparent),
  );

  EquatableConfig.stringify = true;

  // Read Environment Variables
  final environment = Environment.fromEnvironment();

  // Configure Logger
  configureLogger(logLevel: environment.logLevel);

  // Configure Database
  await initDatabase(demoMode: environment.demoMode);

  // Bootstrap the Application and then run it
  await bootstrapApp(environment: environment);

  if (environment.demoMode) {
    Logger('main').config('Application is running in Demo Mode');
  }

  if (environment.devMode) {
    Logger('main').config('Application is running in Dev Mode');
  }

  Logger('main').info('Ready');

  runApp(const App());
}
