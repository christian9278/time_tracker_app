import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'bootstrap.dart';
import 'core/cubits/environment/environment_cubit.dart';
import 'core/widgets/gradient_background_scaffold.dart';
import 'features/history/presentation/screens/history_screen.dart';
import 'features/reports/presentation/screens/reports_screen.dart';
import 'features/settings/presentation/screens/settings_screen.dart';
import 'features/track_time/presentation/screens/track_time_screen.dart';
import 'themes.dart';

/// The home screen for the application.
class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  final _children = [
    const TrackTimeScreen(),
    const HistoryScreen(),
    const ReportsScreen(),
    const SettingsScreen(),
  ];

  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<EnvironmentCubit>(
      create: (_) => serviceLocator<EnvironmentCubit>(),
      child: MaterialApp(
        initialRoute: '/',
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        routes: {
          '/': (context) => GradientBackgroundScaffold(
                body: _children[_currentIndex],
                bottomNavigationBar: _buildBottomNavigationBar(context),
              ),
        },
        supportedLocales: AppLocalizations.supportedLocales,
        title: 'Time Tracker',
        theme: kDefaultTheme,
      ),
    );
  }

  Widget _buildBottomNavigationBar(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.transparent,
      currentIndex: _currentIndex,
      elevation: 0.0,
      items: [
        BottomNavigationBarItem(
          icon: const Icon(CupertinoIcons.timer),
          label: AppLocalizations.of(context).trackTime,
        ),
        BottomNavigationBarItem(
          icon: const Icon(CupertinoIcons.list_bullet),
          label: AppLocalizations.of(context).history,
        ),
        BottomNavigationBarItem(
          icon: const Icon(CupertinoIcons.chart_bar_alt_fill),
          label: AppLocalizations.of(context).reports,
        ),
        BottomNavigationBarItem(
          icon: const Icon(CupertinoIcons.settings),
          label: AppLocalizations.of(context).settings,
        ),
      ],
      onTap: (index) => setState(() => _currentIndex = index),
      selectedItemColor: const Color.fromARGB(255, 169, 194, 217),
      showUnselectedLabels: true,
      type: BottomNavigationBarType.fixed,
      unselectedItemColor: Colors.white,
    );
  }
}
