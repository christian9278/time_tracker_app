// ignore_for_file: constant_identifier_names

// Environment Variables, which can be passed into the Application

/// Environment variable used for demo mode.
const kDEMO_MODE_ENV = 'DEMO_MODE';

/// Environment variable used for development mode.
const kDEV_MODE_ENV = 'DEV_MODE';

/// Environment variable used for custom log level.
const kLOG_LEVEL_ENV = 'LOG_LEVEL';

// Database Constants

/// The hive database name for the application.
const kHiveLocalDatabaseName = 'time_track';

/// the hive database name used for the application, when in demo mode.
const kHiveDemoDatabaseName = 'time_track_demo';
