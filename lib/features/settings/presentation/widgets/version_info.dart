import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

/// Version info widget.
///
/// Shows the version in the format major.minor.patch.build
class VersionInfo extends StatelessWidget {
  /// The [packageInfo], where the version is kept
  final PackageInfo packageInfo;

  const VersionInfo({Key? key, required this.packageInfo}) : super(key: key);

  /// Returns the version and build number as [String].
  ///
  /// If not version or build number is found,
  /// version will be set to 0.0.0 and build number to 0.
  String get appVersion {
    final version =
        packageInfo.version.isNotEmpty ? packageInfo.version : '0.0.0';

    final buildNumber =
        packageInfo.buildNumber.isNotEmpty ? packageInfo.buildNumber : '0';

    return 'v$version+$buildNumber';
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Text(appVersion, style: theme.textTheme.titleMedium);
  }
}
