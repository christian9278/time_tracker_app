import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// A clock widget, which shows the current date and time.
///
/// The widget shows the time in the hh:mm format, without
/// seconds to avoid excessive rendering.
class Clock extends StatefulWidget {
  const Clock({Key? key}) : super(key: key);

  @override
  State<Clock> createState() => _ClockState();
}

class _ClockState extends State<Clock> {
  late DateTime dateTime;
  late StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();

    _subscription = Stream.periodic(const Duration(seconds: 1)).listen(
      // coverage:ignore-start
      (_) {
        if (dateTime.minute != DateTime.now().minute) {
          // Only emit every minute to reduce redering
          setState(() => dateTime = DateTime.now());
        }
      },
      // coverage:ignore-end
    );

    // Set initial DateTime
    setState(() => dateTime = DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Column(
      children: [
        Text(
          DateFormat.jm(Localizations.localeOf(context).languageCode)
              .format(dateTime),
          style: theme.textTheme.displayMedium!.copyWith(
            fontWeight: FontWeight.w200,
          ),
        ),
        Text(
          '''${DateFormat.EEEE(Localizations.localeOf(context).languageCode).format(dateTime)}'''
          ', '
          '''${DateFormat.yMMMMd(Localizations.localeOf(context).languageCode).format(dateTime)}''',
          style: theme.textTheme.headlineSmall!.copyWith(
            fontWeight: FontWeight.w300,
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
}
