import 'dart:async';

import 'package:flutter/material.dart';

import '../../../../core/extensions/duration_extensions.dart';

/// A stopwatch widget.
///
/// The stopwatch can start at zero or at any
/// other provided starting point.
class Stopwatch extends StatefulWidget {
  /// The initial duration, where the stopwatch will start
  final Duration initial;

  const Stopwatch({
    Key? key,
    Duration? initial,
    // ignore: unnecessary_this
  })  : this.initial = initial ?? const Duration(),
        super(key: key);

  @override
  State<Stopwatch> createState() => _StopwatchState();
}

class _StopwatchState extends State<Stopwatch> {
  late Duration duration;
  late StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();

    final startDateTime = DateTime.now().subtract(widget.initial);

    _subscription = Stream.periodic(const Duration(seconds: 1)).listen((_) {
      setState(() => duration = DateTime.now().difference(startDateTime));
    });

    // Set first Duration manually
    setState(() => duration = DateTime.now().difference(startDateTime));
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Text(
      duration.toHoursMinutesSeconds(),
      style:
          theme.textTheme.headlineMedium!.copyWith(fontWeight: FontWeight.w100),
    );
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
}
