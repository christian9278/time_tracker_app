import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../core/entities/time_track_record.dart';
import '../cubits/track_time/track_time_cubit.dart';
import 'stopwatch.dart';

/// The control panel widget for the main screen.
///
/// [ControlPanel] provides four actions for the user,
/// to start, stop, pause and resume time measurements.
class ControlPanel extends StatelessWidget {
  /// The callback, which executes, when pause is clicked
  final VoidCallback onPause;

  /// The callback, which executes, when resume is clicked
  final VoidCallback onResume;

  /// The callback, which executes, when start is clicked
  final VoidCallback onStart;

  /// The callback, which executes, when stop is clicked
  final VoidCallback onStop;

  const ControlPanel({
    Key? key,
    required this.onPause,
    required this.onResume,
    required this.onStart,
    required this.onStop,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white.withOpacity(0.5), width: 10.0),
        borderRadius: BorderRadius.circular(1000.0),
        color: theme.colorScheme.secondary.withOpacity(0.01),
      ),
      height: 275.0,
      width: 275.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          BlocBuilder<TrackTimeCubit, TrackTimeState>(
            builder: (context, state) {
              if (state.isActive && !state.isPaused) {
                return IconButton(
                  key: const Key('control-panel-pause-button'),
                  icon: const Icon(Icons.pause),
                  iconSize: 72.0,
                  onPressed: onPause,
                  padding: const EdgeInsets.only(top: 15.0, bottom: 5.0),
                );
              } else {
                return IconButton(
                  key: const Key('control-panel-start-button'),
                  icon: const Icon(Icons.play_arrow),
                  iconSize: 72.0,
                  onPressed: state.isPaused ? onResume : onStart,
                  padding: const EdgeInsets.only(top: 15.0, bottom: 5.0),
                );
              }
            },
          ),
          BlocBuilder<TrackTimeCubit, TrackTimeState>(
            builder: (context, state) {
              if (state.isPaused) {
                return Text(
                  AppLocalizations.of(context).trackTime_pause,
                  style: theme.textTheme.headlineMedium!
                      .copyWith(fontWeight: FontWeight.w100),
                );
              }

              if (state.isActive) {
                final initialDuration =
                    (state.activeTimeTrackRecord as Some<TimeTrackRecord>)
                        .value
                        .totalDuration;

                return Stopwatch(initial: initialDuration);
              }

              return Text(
                AppLocalizations.of(context).trackTime_hello,
                style: theme.textTheme.headlineMedium!
                    .copyWith(fontWeight: FontWeight.w100),
              );
            },
          ),
          BlocBuilder<TrackTimeCubit, TrackTimeState>(
            builder: (context, state) {
              return IconButton(
                key: const Key('control-panel-stop-button'),
                icon: const Icon(Icons.stop),
                iconSize: 72.0,
                onPressed: state.isActive ? onStop : null,
                padding: const EdgeInsets.only(top: 5.0, bottom: 15.0),
              );
            },
          ),
        ],
      ),
    );
  }
}
