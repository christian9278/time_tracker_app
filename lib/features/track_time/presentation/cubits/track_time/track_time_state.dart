part of 'track_time_cubit.dart';

/// The abstract base state.
///
/// The base state holds either the active [TimeTrackRecord]
/// or [None], of no time measurement is active.
@immutable
abstract class TrackTimeState extends Equatable {
  /// The [activeTimeTrackRecord]
  final Option<TimeTrackRecord> activeTimeTrackRecord;

  const TrackTimeState({required this.activeTimeTrackRecord});

  /// Returns `true`, if a measurement is running
  bool get isActive => activeTimeTrackRecord.fold(
        () => false,
        (record) => record.state != TimeTrackRecordState.finished,
      );

  /// Returns `true`, if the current measurement is paused
  bool get isPaused => activeTimeTrackRecord.fold(
        () => false,
        (record) => record.state == TimeTrackRecordState.paused,
      );

  @override
  List<Object?> get props =>
      [activeTimeTrackRecord.fold(() => null, (record) => record)];
}

/// The initial state.
@immutable
class TrackTimeInitial extends TrackTimeState {
  const TrackTimeInitial() : super(activeTimeTrackRecord: const None());
}

/// The state which emits, if the state of the current [TimeTrackRecord] changes
/// or a measurement was created or finished.
@immutable
class TrackTimeStateChange extends TrackTimeState {
  const TrackTimeStateChange({
    required Option<TimeTrackRecord> activeTimeTrackRecord,
  }) : super(activeTimeTrackRecord: activeTimeTrackRecord);
}
