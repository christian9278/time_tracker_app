import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../../../../core/entities/time_track_record.dart';
import '../../../../../core/entities/time_track_record_state.dart';
import '../../../domain/repositories/track_time_repository.dart';

part 'track_time_state.dart';

/// The cubit for the [TrackTimeScreen].
class TrackTimeCubit extends Cubit<TrackTimeState> {
  /// The [trackTimeRepository]
  final TrackTimeRepository trackTimeRepository;

  late StreamSubscription _trackChangeSubscription;

  TrackTimeCubit({required this.trackTimeRepository})
      : super(const TrackTimeInitial());

  /// Initializes the cubit and emits the first [TrackTimeStateChange].
  Future<void> initialize() async {
    _trackChangeSubscription = trackTimeRepository.active.listen((record) {
      emit(TrackTimeStateChange(activeTimeTrackRecord: record));
    });
  }

  /// Pauses the current time measurement and emits a [TrackTimeStateChange].
  Future<void> pause() async {
    final record = (state.activeTimeTrackRecord as Some).value;
    await trackTimeRepository.pause(record);
  }

  /// Resumes the current time measurement and emits a [TrackTimeStateChange].
  Future<void> resume() async {
    final record = (state.activeTimeTrackRecord as Some).value;
    await trackTimeRepository.resume(record);
  }

  /// Starts a new time measurement and emits a [TrackTimeStateChange].
  Future<void> start() async => await trackTimeRepository.create();

  /// Stops the current time measurement and emits a [TrackTimeStateChange].
  Future<void> stop() async {
    final record = (state.activeTimeTrackRecord as Some).value;
    await trackTimeRepository.finish(record);
  }

  @override
  Future<void> close() {
    _trackChangeSubscription.cancel();
    return super.close();
  }
}
