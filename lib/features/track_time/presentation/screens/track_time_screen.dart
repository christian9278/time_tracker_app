import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../bootstrap.dart';
import '../cubits/track_time/track_time_cubit.dart';
import '../widgets/clock.dart';
import '../widgets/control_panel.dart';
import '../../../../core/widgets/loading.dart';

/// The track time main screen.
class TrackTimeScreen extends StatelessWidget {
  const TrackTimeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TrackTimeCubit>(
      create: (_) => serviceLocator<TrackTimeCubit>(),
      child: _buildBody(context),
    );
  }

  SafeArea _buildBody(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          const SizedBox(height: 15.0),
          const Clock(),
          BlocBuilder<TrackTimeCubit, TrackTimeState>(
            builder: (context, state) {
              if (state is TrackTimeInitial) {
                return const Expanded(
                  child: Center(
                    child: Loading(),
                  ),
                );
              } else {
                return Expanded(
                  child: Center(
                    child: ControlPanel(
                      onPause: () => context.read<TrackTimeCubit>().pause(),
                      onResume: () => context.read<TrackTimeCubit>().resume(),
                      onStart: () => context.read<TrackTimeCubit>().start(),
                      onStop: () => context.read<TrackTimeCubit>().stop(),
                    ),
                  ),
                );
              }
            },
          ),
          const SizedBox(height: 15.0),
        ],
      ),
    );
  }
}
