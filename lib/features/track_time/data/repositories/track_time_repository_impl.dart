import 'package:dartz/dartz.dart';
import 'package:rxdart/rxdart.dart';
import 'package:time_tracker_app/core/exceptions/record_exists_exception.dart';
import 'package:time_tracker_app/core/failures/record_exists_failure.dart';
import 'package:time_tracker_app/core/failures/record_not_found_failure.dart';

import '../../../../core/datasources/hive_datasource.dart';
import '../../../../core/entities/time_track_record.dart';
import '../../../../core/entities/time_track_record_state.dart';
import '../../../../core/exceptions/record_not_found_exception.dart';
import '../../../../core/failures/failure.dart';
import '../../../../core/failures/state_failure.dart';
import '../../../../core/models/time_track_record_model.dart';
import '../../domain/repositories/track_time_repository.dart';

/// The repository implementation for all time track measurement related operations.
class TrackTimeRepositoryImpl implements TrackTimeRepository {
  /// The datasource for the Hive database
  final HiveDatasource datasource;

  final _subject$ = BehaviorSubject<Option<TimeTrackRecord>>();

  TrackTimeRepositoryImpl({required this.datasource});

  /// Initializes the repository.
  ///
  /// Must be called before any other operation.
  void initialize() {
    final hasActiveTimeTrackRecord = datasource.values.any(
      (model) => model.state != TimeTrackRecordState.finished.index,
    );

    if (hasActiveTimeTrackRecord) {
      final model = datasource.values.singleWhere(
          (model) => model.state != TimeTrackRecordState.finished.index);

      _subject$.add(Some(model.toEntity()));
    } else {
      _subject$.add(const None());
    }
  }

  @override
  Stream<Option<TimeTrackRecord>> get active => _subject$.stream;

  @override
  Future<Either<Failure, TimeTrackRecord>> create() async {
    final hasActiveTimeTrackRecord = (await _subject$.first).isSome();
    if (hasActiveTimeTrackRecord) {
      return const Left(
        StateFailure(
          '''Can't create TimeTrackRecord. Only one active TimeTrackRecord is allowed''',
        ),
      );
    }

    try {
      final newRecord = TimeTrackRecord.create();
      await datasource.add(TimeTrackRecordModel.fromEntity(newRecord));

      _subject$.add(Some(newRecord));
      return Right(newRecord);
    } on RecordExistsException catch (e) {
      return Left(RecordExistsFailure(
        e.message,
        exception: e,
      ));
    }
  }

  @override
  Future<Either<Failure, Unit>> finish(TimeTrackRecord record) async {
    if (!record.isActive) {
      return const Left(
        StateFailure('TimeTrackRecord already finished'),
      );
    }

    final finishedRecord = record.finish();
    return finishedRecord.fold(
      (failure) => Left(failure), // coverage:ignore-line
      (record) async {
        try {
          await datasource.put(TimeTrackRecordModel.fromEntity(record));

          _subject$.add(const None());
          return const Right(unit);
        } on RecordNotFoundException catch (e) {
          return Left(RecordNotFoundFailure(
            e.message,
            exception: e,
          ));
        }
      },
    );
  }

  @override
  Future<Either<Failure, TimeTrackRecord>> pause(TimeTrackRecord record) async {
    final pausedRecord = record.pause();
    return pausedRecord.fold(
      (failure) => Left(failure),
      (record) async {
        try {
          await datasource.put(TimeTrackRecordModel.fromEntity(record));

          _subject$.add(Some(record));
          return Right(record);
        } on RecordNotFoundException catch (e) {
          return Left(
            RecordNotFoundFailure(e.message, exception: e),
          );
        }
      },
    );
  }

  @override
  Future<Either<Failure, TimeTrackRecord>> resume(
    TimeTrackRecord record,
  ) async {
    final resumedRecord = record.resume();
    return resumedRecord.fold(
      (failure) => Left(failure),
      (record) async {
        try {
          await datasource.put(TimeTrackRecordModel.fromEntity(record));

          _subject$.add(Some(record));
          return Right(record);
        } on RecordNotFoundException catch (e) {
          return Left(
            RecordNotFoundFailure(e.message, exception: e),
          );
        }
      },
    );
  }

  @override
  Future<void> dispose() async {
    await _subject$.close();
    await datasource.dispose();
  }
}
