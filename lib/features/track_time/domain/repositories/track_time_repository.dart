import 'package:dartz/dartz.dart';

import '../../../../core/entities/time_track_record.dart';
import '../../../../core/failures/failure.dart';

/// The interface for the [TrackTimeRepository].
abstract class TrackTimeRepository {
  /// Returns a [Stream], which publishes either the active [TimeTrackRecord]
  /// or [None], if no measurement is active.
  Stream<Option<TimeTrackRecord>> get active;

  /// Creates a new [TimeTrackRecord] and starts a time measurement.
  ///
  /// Returns a [Future], which will resolve in either the
  /// created [TimeTrackRecord] or a [Failure].
  Future<Either<Failure, TimeTrackRecord>> create();

  /// Finishes the [TimeTrackRecord] measurement.
  ///
  /// Returns a [Future], which will resolve either in a
  /// [Unit], if the operation was successful, or a [Failure].
  Future<Either<Failure, Unit>> finish(TimeTrackRecord record);

  /// Pauses the [TimeTrackRecord] measurement.
  ///
  /// Returns a [Future], which will resolve either in a
  /// updated [TimeTrackRecord] or a [Failure], if the measurement
  /// cannot be paused.
  Future<Either<Failure, TimeTrackRecord>> pause(TimeTrackRecord record);

  /// Resumes a paused [TimeTrackRecord] measurement.
  ///
  /// Returns a [Future], which will resolve either in a
  /// updated [TimeTrackRecord] or a [Failure], if the measurement
  /// cannott resume.
  Future<Either<Failure, TimeTrackRecord>> resume(TimeTrackRecord record);

  /// Disposes the instance.
  Future<void> dispose();
}
