import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/// The report screen, which shows a variety of reports.
class ReportsScreen extends StatelessWidget {
  const ReportsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: _buildAppBar(context),
        body: Container(),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    final theme = Theme.of(context);

    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      primary: false,
      title: Text(
        AppLocalizations.of(context).reports.toUpperCase(),
        style: theme.textTheme.headlineSmall!.copyWith(
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }
}
