import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/// The history screen, which shows all past
/// and currently active time measurements.
class HistoryScreen extends StatelessWidget {
  const HistoryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: _buildAppBar(context),
        body: Container(),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    final theme = Theme.of(context);

    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      primary: false,
      title: Text(
        AppLocalizations.of(context).history.toUpperCase(),
        style: theme.textTheme.headlineSmall!.copyWith(
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }
}
