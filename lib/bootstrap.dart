import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:logging/logging.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:platform/platform.dart';

import 'constants.dart';
import 'core/cubits/environment/environment_cubit.dart';
import 'core/datasources/hive_datasource.dart';
import 'core/datasources/hive_datasource_impl.dart';
import 'core/entities/environment.dart';
import 'features/track_time/data/repositories/track_time_repository_impl.dart';
import 'features/track_time/domain/repositories/track_time_repository.dart';
import 'features/track_time/presentation/cubits/track_time/track_time_cubit.dart';

// The Service Locator is a global registered variable in the app.
final serviceLocator = GetIt.instance;

/// Bootstrap the application.
///
/// Registers all objects in the dependency injection container.
/// Takes an [Environment] parameter to find out, if the application
/// runs in demo and / or in dev mode.
Future<void> bootstrapApp({
  Environment environment = const Environment(),
}) async {
  Logger('bootstrapApp').config('Bootstrapping Application');

  await _registerExternalDependencies();
  _registerDatasources(demoMode: environment.demoMode);
  _registerRepositories();
  _registerCubits(environment: environment);

  await serviceLocator.allReady();
}

Future<void> _registerExternalDependencies() async {
  serviceLocator.registerSingletonAsync<PackageInfo>(
    () async => PackageInfo.fromPlatform(),
  );

  serviceLocator.registerLazySingleton<Platform>(() => const LocalPlatform());
}

void _registerDatasources({bool demoMode = false}) {
  serviceLocator.registerLazySingleton<HiveDatasource>(
    () => HiveLocalDatasourceImpl.create(
      Hive,
      databaseName: demoMode ? kHiveDemoDatabaseName : kHiveLocalDatabaseName,
    ),
  );
}

void _registerRepositories() {
  serviceLocator.registerLazySingleton<TrackTimeRepository>(
    () => TrackTimeRepositoryImpl(
      datasource: serviceLocator<HiveDatasource>(),
    )..initialize(),
  );
}

void _registerCubits({required Environment environment}) {
  serviceLocator.registerFactory<EnvironmentCubit>(
    () => EnvironmentCubit(
      environment: environment,
      packageInfo: serviceLocator<PackageInfo>(),
      platform: serviceLocator<Platform>(),
    ),
  );

  serviceLocator.registerFactory<TrackTimeCubit>(
    () => TrackTimeCubit(
      trackTimeRepository: serviceLocator<TrackTimeRepository>(),
    )..initialize(),
  );
}
