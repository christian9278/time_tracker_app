import 'dart:developer';

import 'package:logging/logging.dart';

/// Configures the logger.
///
/// Configures the root logger for the application and
/// sets the [logLevel] to 800, if no other value is provided.
///
/// For a list of possible values, see:
/// https://pub.dev/documentation/logging/latest/logging/Level-class.html
void configureLogger({int logLevel = 800}) {
  Logger.root.level = Level('CUSTOM', logLevel);
  Logger.root.onRecord.listen(
    (record) => log(
      record.message,
      time: record.time,
      sequenceNumber: record.sequenceNumber,
      level: record.level.value,
      name: record.level.name,
      zone: record.zone,
      error: record.error,
      stackTrace: record.stackTrace,
    ),
  );
}
