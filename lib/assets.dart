/// Used assets in the application.
mixin Assets {
  static const images = {
    'img1': 'lib/assets/images/undraw_time_management_30iu.svg',
    'img2': 'lib/assets/images/undraw_in_no_time_6igu.svg',
  };
}
