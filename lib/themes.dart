import 'package:flutter/material.dart';

/// The default theme for the application.
final kDefaultTheme = ThemeData(
  colorScheme: const ColorScheme.dark(
    primary: Color.fromARGB(255, 86, 101, 115),
    secondary: Colors.white,
    brightness: Brightness.dark,
  ),
  fontFamily: 'Roboto',
  scaffoldBackgroundColor: Colors.transparent,
);
