import 'package:flutter/foundation.dart';
import 'package:time_tracker_app/core/failures/failure.dart';

/// Failure, if a database record already exists.
@immutable
class RecordExistsFailure extends Failure {
  const RecordExistsFailure(String message, {required Exception exception})
      : super(message, exception: exception);
}
