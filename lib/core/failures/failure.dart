import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

/// The abstract base class for all kinds of failures
/// in the application.
@immutable
abstract class Failure extends Equatable {
  /// The failure message
  final String message;

  /// The optional excpetion
  final Exception? exception;

  /// The optional stack trace
  final StackTrace? stackTrace;

  const Failure(this.message, {this.exception, this.stackTrace});

  // coverage:ignore-start
  @override
  List<Object?> get props => [message, exception, stackTrace];
  // coverage:ignore-end
}
