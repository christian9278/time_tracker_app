import 'package:flutter/material.dart';

import 'failure.dart';

/// The failure indicates an inconsistant state
/// in a [TimeTrackRecord] or [TimeTrackFragment] entity.
@immutable
class StateFailure extends Failure {
  const StateFailure(String message) : super(message);
}
