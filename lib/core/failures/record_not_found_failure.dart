import 'package:flutter/foundation.dart';

import 'failure.dart';

/// Failure, if a database record was not found.
@immutable
class RecordNotFoundFailure extends Failure {
  const RecordNotFoundFailure(String message, {required Exception exception})
      : super(message, exception: exception);
}
