import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import '../entities/time_track_record.dart';
import '../entities/time_track_record_state.dart';
import 'time_track_fragment_model.dart';

part 'time_track_record_model.g.dart';

/// The database model for the [TimeTrackRecord] entity.
@HiveType(typeId: 0)
@immutable
class TimeTrackRecordModel extends Equatable {
  /// The unique identifier
  @HiveField(0)
  final String uuid;

  /// A [List] of [TimeTrackFragmentModel]
  @HiveField(1)
  final List<TimeTrackFragmentModel> fragments;

  /// The current state
  @HiveField(2)
  final int state;

  const TimeTrackRecordModel({
    required this.uuid,
    required this.fragments,
    required this.state,
  });

  /// Converts a [TimeTrackRecord] entity to [TimeTrackRecordModel].
  factory TimeTrackRecordModel.fromEntity(TimeTrackRecord entity) {
    return TimeTrackRecordModel(
      uuid: entity.uuid,
      fragments: entity.fragments
          .map((item) => TimeTrackFragmentModel.fromEntity(item))
          .toList(),
      state: entity.state.index,
    );
  }

  /// Converts a [TimeTrackRecordModel] to a [TimeTrackRecord] entity.
  TimeTrackRecord toEntity() {
    return TimeTrackRecord(
      uuid: uuid,
      fragments: fragments.map((item) => item.toEntity()).toList(),
      state: TimeTrackRecordState.values[state],
    );
  }

  // coverage:ignore-start
  @override
  List<Object?> get props => [uuid, fragments, state];
  // coverage:ignore-end
}
