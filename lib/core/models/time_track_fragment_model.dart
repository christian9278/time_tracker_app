import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import '../entities/time_track_fragment.dart';

part 'time_track_fragment_model.g.dart';

/// The database model for the [TimeTrackFragment] entity.
@HiveType(typeId: 1)
@immutable
class TimeTrackFragmentModel extends Equatable {
  /// The [startDate] of the measurement
  @HiveField(1)
  final DateTime startDate;

  /// The [endDate] of the measurement or [Null], if the
  /// measurement ist still active
  @HiveField(2)
  final DateTime? endDate;

  /// The [duration] of the measurement or [Null], if the
  /// measurement is still active
  @HiveField(3)
  final int? duration;

  const TimeTrackFragmentModel({
    required this.startDate,
    required this.endDate,
    required this.duration,
  });

  /// Converts a [TimeTrackFragment] entity to [TimeTrackFragmentModel].
  factory TimeTrackFragmentModel.fromEntity(TimeTrackFragment entity) {
    return TimeTrackFragmentModel(
        startDate: entity.startDate,
        endDate: entity.endDate.fold(() => null, (endDate) => endDate),
        duration: entity.duration
            .fold(() => null, (duration) => duration.inMilliseconds));
  }

  /// Converts a [TimeTrackFragmentModel] to a [TimeTrackFragment] entity.
  TimeTrackFragment toEntity() {
    return TimeTrackFragment(
      startDate: startDate,
      endDate: endDate != null ? Some(endDate!) : const None(),
      duration: duration != null
          ? Some(Duration(milliseconds: duration!))
          : const None(),
    );
  }

  @override
  List<Object?> get props => [startDate, endDate, duration];
}
