import '../models/time_track_record_model.dart';

/// The datasource interface for the Hive Database.
///
/// The datasource is used for all read and write
/// operations with the hive database.
abstract class HiveDatasource {
  /// Returns a [List] of all [TimeTrackRecordModel]
  List<TimeTrackRecordModel> get values;

  /// Adds a new [TimeTrackRecordModel] to the database.
  ///
  /// Returns a [Future], which will be resolved,
  /// if the operation was successful, otherwise rejected.
  Future<void> add(TimeTrackRecordModel model);

  /// Updates an existing [TimeTrackRecordModel] in the database.
  ///
  /// Returns a [Future], which will be resolved,
  /// if the operation was successful, otherwise rejected.
  Future<void> put(TimeTrackRecordModel model);

  /// Disposes the object.
  ///
  /// Frees all resources and closes the connection to the database.
  Future<void> dispose();
}
