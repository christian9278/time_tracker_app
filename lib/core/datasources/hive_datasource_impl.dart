import 'package:hive/hive.dart';
import 'package:time_tracker_app/core/exceptions/record_exists_exception.dart';
import 'package:time_tracker_app/core/exceptions/record_not_found_exception.dart';

import '../models/time_track_record_model.dart';
import 'hive_datasource.dart';

/// The local datasource for the hive database.
class HiveLocalDatasourceImpl implements HiveDatasource {
  /// The used hive box.
  final Box<TimeTrackRecordModel> box;

  /// Creates the datasource object.
  ///
  /// Takes a [hive] object and the [databaseName]
  /// of the database to connect as parameters and returns
  /// a [HiveLocalDatasourceImpl] instance.
  factory HiveLocalDatasourceImpl.create(
    HiveInterface hive, {
    required String databaseName,
  }) {
    final box = hive.box<TimeTrackRecordModel>(databaseName);
    return HiveLocalDatasourceImpl._(box: box);
  }

  HiveLocalDatasourceImpl._({required this.box});

  @override
  List<TimeTrackRecordModel> get values => box.values.toList();

  @override
  Future<void> add(TimeTrackRecordModel model) async {
    if (box.keys.any((uuid) => uuid == model.uuid)) {
      throw const RecordExistsException();
    }

    await box.put(model.uuid, model);
  }

  @override
  Future<void> put(TimeTrackRecordModel model) async {
    if (!box.keys.any((uuid) => uuid == model.uuid)) {
      throw const RecordNotFoundException();
    }

    await box.put(model.uuid, model);
  }

  @override
  Future<void> dispose() async {
    await box.compact();
    await box.close();
  }
}
