import 'package:intl/intl.dart';

/// Extension methods on the [DateTime] class.
extension DateTimeExtensions on DateTime {
  /// Returns the week number of the [DateTime] as [int]
  int get week {
    final dayOfYear = int.parse(DateFormat("D").format(this));
    final weekOfYear = ((dayOfYear - weekday + 10) / 7).floor();

    return weekOfYear < 1 ? 52 : weekOfYear;
  }

  /// Returns `true`, if the [DateTime] is today.
  bool get isToday {
    final today = DateTime.now();
    return day == today.day && month == today.month && year == today.year;
  }

  /// Returns `true`, if the [DateTime] was yesterday.
  bool get isYesterday {
    final yesterday = DateTime.now().subtract(const Duration(days: 1));
    return day == yesterday.day &&
        month == yesterday.month &&
        year == yesterday.year;
  }

  /// Returns `true`, if the [DateTime] is in the current week.
  bool get isInWeek => week == DateTime.now().week;

  /// Returns `true`, if the [DateTime] is in the current month.
  bool get isInMonth => month == DateTime.now().month;

  /// Returns `true`, if the [DateTime] is in the current year.
  bool get isInYear => year == DateTime.now().year;

  // Check if this date is on a leap year
  bool get isLeapYear => year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}
