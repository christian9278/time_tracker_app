import 'package:flutter/material.dart';

/// A background image widget, which shows a
/// gradient from `Theme.primary` to `Theme.secondary`.
class GradientBackground extends StatelessWidget {
  /// The child [Widget]
  final Widget child;

  const GradientBackground({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        Positioned(
          top: 0.0,
          right: 0.0,
          bottom: 0.0,
          left: 0.0,
          child: _buildGradient(context),
        ),
        Positioned(top: 0.0, right: 0.0, bottom: 0.0, left: 0.0, child: child),
      ],
    );
  }

  Widget _buildGradient(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      decoration: BoxDecoration(
        color: theme.colorScheme.primary,
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            theme.colorScheme.primary,
            theme.colorScheme.secondary.withAlpha(0)
          ],
        ),
      ),
    );
  }
}
