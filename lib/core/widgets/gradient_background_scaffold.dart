import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../cubits/environment/environment_cubit.dart';
import 'gradient_background.dart';

/// Enhanced [Scaffold] widget.
///
/// Uses a [GradientBackground] widget as background and gives
/// access to [EnvironmentCubit]. Depending on the state of the
/// [EnvironmentCubit], the [GradientBackgroundScaffold] also shows
/// [Banner] for `demoMode` and `devMode`, if set.
class GradientBackgroundScaffold extends StatelessWidget {
  /// An app bar to display at the top of the scaffold.
  final PreferredSizeWidget? appBar;

  /// The primary content of the scaffold.
  final Widget body;

  /// A bottom navigation bar to display at the bottom of the scaffold.
  final Widget? bottomNavigationBar;

  /// If true the [body] and the scaffold's floating widgets should size
  /// themselves to avoid the onscreen keyboard whose height is defined by the
  /// ambient [MediaQuery]'s [MediaQueryData.viewInsets] `bottom` property.
  ///
  /// For example, if there is an onscreen keyboard displayed above the
  /// scaffold, the body can be resized to avoid overlapping the keyboard, which
  /// prevents widgets inside the body from being obscured by the keyboard.
  ///
  /// Defaults to true.
  final bool? resizeToAvoidBottomInset;

  const GradientBackgroundScaffold({
    Key? key,
    this.appBar,
    required this.body,
    this.bottomNavigationBar,
    this.resizeToAvoidBottomInset,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EnvironmentCubit, EnvironmentState>(
      builder: (context, state) {
        final appBanner = <Banner>[];

        // Banner for Demo Mode
        if (state.environment.demoMode) {
          appBanner.add(
            Banner(
              message: AppLocalizations.of(context).demoMode.toUpperCase(),
              location: BannerLocation.bottomStart,
              color: Colors.green.withOpacity(0.6),
            ),
          );
        }

        // Banner for Dev Mode
        if (state.environment.devMode) {
          appBanner.add(
            Banner(
              message: AppLocalizations.of(context).devMode.toUpperCase(),
              location: BannerLocation.bottomEnd,
              color: Colors.orange.withOpacity(0.6),
            ),
          );
        }

        return Scaffold(
          appBar: appBar,
          body: Stack(
            fit: StackFit.expand,
            children: [
              ...appBanner,
              GradientBackground(child: body),
            ],
          ),
          bottomNavigationBar: bottomNavigationBar,
          extendBody: true,
          extendBodyBehindAppBar: true,
          resizeToAvoidBottomInset: resizeToAvoidBottomInset,
        );
      },
    );
  }
}
