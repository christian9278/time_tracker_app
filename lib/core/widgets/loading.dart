import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/// An animated loading widget.
///
/// The [StatefulWidget] shows an animated circle with a
/// loading message.
class Loading extends StatefulWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  State<Loading> createState() => _LoadingState();
}

class _LoadingState extends State<Loading> with SingleTickerProviderStateMixin {
  late final Animation _circleAnimation;
  late final Animation _textAnimation;
  late final AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );

    _circleAnimation =
        Tween(begin: 0.2, end: 0.5).animate(_animationController);
    _textAnimation = Tween(begin: 0.5, end: 1.0).animate(_animationController);

    _animationController.repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return AnimatedBuilder(
        animation: _animationController,
        builder: (context, snapshot) {
          return Ink(
            decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.white.withOpacity(_circleAnimation.value),
                  width: 10.0),
              borderRadius: BorderRadius.circular(1000.0),
              color: theme.colorScheme.secondary.withOpacity(0.01),
            ),
            height: 275.0,
            width: 275.0,
            child: Center(
              child: Opacity(
                opacity: _textAnimation.value,
                child: Text(
                  AppLocalizations.of(context).loading,
                  textAlign: TextAlign.center,
                  style: theme.textTheme.headlineMedium!
                      .copyWith(fontWeight: FontWeight.w100),
                ),
              ),
            ),
          );
        });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
