import 'package:flutter/material.dart';

/// The [RecordNotFoundException] will be thrown,
/// if a record was not found in the database.
@immutable
class RecordNotFoundException implements Exception {
  /// The error message
  final String message;

  const RecordNotFoundException([this.message = ""]);
}
