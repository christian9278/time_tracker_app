import 'package:flutter/material.dart';

/// The [RecordExistsException] will be thrown,
/// if a record with the same identifier already exists
/// in the database.
@immutable
class RecordExistsException implements Exception {
  /// The error message
  final String message;

  const RecordExistsException([this.message = ""]);
}
