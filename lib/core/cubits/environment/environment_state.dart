part of 'environment_cubit.dart';

/// The environment state.
///
/// The environment state stores information about
/// the current environment variables, package
/// and platform related information.
@immutable
abstract class EnvironmentState extends Equatable {
  /// The current environment variables
  final Environment environment;

  /// Information about the package
  final PackageInfo packageInfo;

  /// Information about the current platform
  final Platform platform;

  const EnvironmentState({
    required this.environment,
    required this.packageInfo,
    required this.platform,
  });

  @override
  List<Object> get props => [environment, packageInfo, platform];
}

/// The initial environment state.
@immutable
class EnvironmentInitial extends EnvironmentState {
  const EnvironmentInitial({
    required Environment environment,
    required PackageInfo packageInfo,
    required Platform platform,
  }) : super(
          environment: environment,
          packageInfo: packageInfo,
          platform: platform,
        );
}
