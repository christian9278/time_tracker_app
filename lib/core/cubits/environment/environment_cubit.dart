import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:platform/platform.dart';

import '../../entities/environment.dart';

part 'environment_state.dart';

/// The environment cubit.
///
/// The cubit grants access to environment variables,
/// package and platform related information.
class EnvironmentCubit extends Cubit<EnvironmentState> {
  /// The current environment variables
  final Environment environment;

  /// Information about the package
  final PackageInfo packageInfo;

  /// Information about the current platform
  final Platform platform;

  EnvironmentCubit({
    required this.environment,
    required this.packageInfo,
    required this.platform,
  }) : super(EnvironmentInitial(
          environment: environment,
          packageInfo: packageInfo,
          platform: platform,
        ));
}
