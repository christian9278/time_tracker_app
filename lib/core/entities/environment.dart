import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

/// Represents all possible environment variables,
/// which are accessible within the application.
@immutable
class Environment extends Equatable {
  /// Indicates, if demo mode is active.
  final bool demoMode;

  /// Indicates, if development mode is active.
  final bool devMode;

  /// The current log level.
  final int logLevel;

  const Environment({
    this.demoMode = false,
    this.devMode = false,
    this.logLevel = 800,
  });

  /// Reads and parses the environment variables and creates
  /// an [Environment] instance.
  ///
  /// If an environment variable is not available, default values
  /// will be used instead.
  factory Environment.fromEnvironment() {
    const demoMode = bool.fromEnvironment(kDEMO_MODE_ENV, defaultValue: false);
    const devMode = bool.fromEnvironment(kDEV_MODE_ENV, defaultValue: false);
    const logLevel = int.fromEnvironment(kLOG_LEVEL_ENV, defaultValue: 800);

    return const Environment(
      demoMode: demoMode,
      devMode: devMode,
      logLevel: logLevel,
    );
  }

  // coverage:ignore-start
  @override
  List<Object> get props => [demoMode, devMode, logLevel];
  // coverage:ignore-end
}
