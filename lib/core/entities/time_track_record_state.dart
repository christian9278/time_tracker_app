/// The possible states for a [TimeTrackRecord].
enum TimeTrackRecordState {
  /// Indicates, that tracking is currently active
  tracking,

  /// Indicates, that tracking is currently paused
  paused,

  /// Indicates, that tracking is finished
  finished,
}
