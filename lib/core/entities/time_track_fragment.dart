import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../failures/failure.dart';
import '../failures/state_failure.dart';

/// Represents a time measurement from [startDate] to [endDate].
///
/// If the measurement is still running, the [endDate], as well
/// as the [duration] return [None].
@immutable
class TimeTrackFragment extends Equatable {
  /// The start [DateTime] of the measurement
  final DateTime startDate;

  /// The end [DateTime] of the measurement or [None], if
  /// the measurement is still active
  final Option<DateTime> endDate;

  /// The [Duration] of the measurement or [None], if the
  /// measurement is still active
  final Option<Duration> duration;

  const TimeTrackFragment({
    required this.startDate,
    required this.endDate,
    required this.duration,
  });

  /// Creates a new time measurement.
  ///
  /// [startDate] is set automatically to the current [DateTime].
  /// [endDate] and [duration] are set to [None].
  factory TimeTrackFragment.create() => TimeTrackFragment(
        startDate: DateTime.now(),
        endDate: const None(),
        duration: const None(),
      );

  /// Returns `true`, if the measurement is still running.
  bool get isActive => endDate.isNone();

  /// Finishes the active time measurement.
  ///
  /// Sets [endDate] the current [DateTime] and [duration] to the
  /// difference between [startDate] and [endDate].
  ///
  /// Returns a new instance with the values of the properties or
  /// a [StateFailure], if the measurement was already finished.
  Either<Failure, TimeTrackFragment> finish() {
    if (this.endDate.isSome()) {
      return const Left(StateFailure('Already finished'));
    }

    final endDate = DateTime.now();

    return Right(TimeTrackFragment(
      startDate: startDate,
      endDate: Some(endDate),
      duration: Some(endDate.difference(startDate)),
    ));
  }

  /// Returns a copy of the current instance.
  TimeTrackFragment copyWith({
    DateTime? startDate,
    Option<DateTime>? endDate,
    Option<Duration>? duration,
  }) {
    return TimeTrackFragment(
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
      duration: duration ?? this.duration,
    );
  }

  @override
  List<Object?> get props => [
        startDate,
        endDate.fold(() => null, (endDate) => endDate),
        duration.fold(() => null, (duration) => duration),
      ];
}
