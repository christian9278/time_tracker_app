import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:time_tracker_app/core/failures/state_failure.dart';
import 'package:uuid/uuid.dart';

import '../failures/failure.dart';
import 'time_track_fragment.dart';
import 'time_track_record_state.dart';

/// Represents a series of [TimeTrackFragment], which make up one
/// consecutive time measurement.
@immutable
class TimeTrackRecord extends Equatable {
  /// The unique identifier for the measurement
  final String uuid;

  /// A [List] of [TimeTrackFragment], which contain the
  /// actual measurements
  final List<TimeTrackFragment> fragments;

  /// The current state of the measurement
  final TimeTrackRecordState state;

  const TimeTrackRecord({
    required this.uuid,
    required this.fragments,
    required this.state,
  });

  /// Creates and starts a new time measurement.
  ///
  /// Returns a [TimeTrackRecord] with a new [Uuid] and sets the
  /// current state to [TimeTrackRecordState.tracking].
  factory TimeTrackRecord.create() => TimeTrackRecord(
        uuid: const Uuid().v4(),
        fragments: [TimeTrackFragment.create()],
        state: TimeTrackRecordState.tracking,
      );

  /// Returns the [endDate] of the whole measurement or
  /// [None], if the measurement is still active.
  Option<DateTime> get endDate => fragments.last.endDate;

  /// Returns `true`, if the measurement is still running,
  /// otherwise `false`.
  bool get isActive => state != TimeTrackRecordState.finished;

  /// Returns `true`, if the measurement is currently paused,
  /// otherwise `false`.
  bool get isPaused => state == TimeTrackRecordState.paused;

  /// Returns the [startDate] of the measurement.
  DateTime get startDate => fragments.first.startDate;

  /// Returns the [totalDuration] over all recorded measurement fragments.
  Duration get totalDuration {
    late final Duration finishedAndPausedFragmentsDuration;
    if (fragments.any((fragment) => fragment.duration.isSome())) {
      finishedAndPausedFragmentsDuration = fragments
          .where((fragment) => fragment.duration.isSome())
          .map<Duration>(
              (fragment) => (fragment.duration as Some<Duration>).value)
          .reduce(
            (value, duration) => Duration(
                milliseconds: value.inMilliseconds + duration.inMilliseconds),
          );
    } else {
      finishedAndPausedFragmentsDuration = const Duration(milliseconds: 0);
    }

    late final Duration activeFragmentDuration;
    if (fragments.any((fragment) => fragment.duration.isNone())) {
      final activeFragment =
          fragments.singleWhere((fragment) => fragment.duration.isNone());

      activeFragmentDuration = DateTime.now().difference(
        activeFragment.startDate,
      );
    } else {
      activeFragmentDuration = const Duration(milliseconds: 0);
    }

    return Duration(
        milliseconds: finishedAndPausedFragmentsDuration.inMilliseconds +
            activeFragmentDuration.inMilliseconds);
  }

  /// Pauses the current measurement.
  ///
  /// Sets the current state to [TimeTrackRecordState.paused] and
  /// returns a [TimeTrackRecord] with the updated values or
  /// a [StateFailure], if the measurement cannot be
  /// paused, because it's already paused or finished.
  Either<Failure, TimeTrackRecord> pause() {
    if (state == TimeTrackRecordState.paused) {
      return const Left(
        StateFailure('Can\'t pause, already paused'),
      );
    }

    if (state == TimeTrackRecordState.finished) {
      return const Left(
        StateFailure('Can\'t pause, already finished'),
      );
    }

    final activeFragment = fragments.singleWhere(
      (item) => item.endDate.isNone(),
    );
    final finishedFragment = (activeFragment.finish() as Right).value;

    return Right(
      copyWith(
        fragments: [
          ...fragments.where((item) => item != activeFragment),
          finishedFragment,
        ],
        state: TimeTrackRecordState.paused,
      ),
    );
  }

  /// Resumes a previously paused measurement.
  ///
  /// Sets the current state to [TimeTrackRecordState.tracking] and
  /// returns a [TimeTrackRecord] with the updated values or
  /// a [StateFailure], if the measurement cannot resume,
  /// because it wasn't paused.
  Either<Failure, TimeTrackRecord> resume() {
    if (state != TimeTrackRecordState.paused) {
      return const Left(StateFailure('Can\'t resume, not paused'));
    }

    return Right(
      copyWith(
        fragments: [...fragments, TimeTrackFragment.create()],
        state: TimeTrackRecordState.tracking,
      ),
    );
  }

  /// Finishes a time measurement.
  ///
  /// Sets the current state to [TimeTrackRecordState.finished] and
  /// returns a [TimeTrackRecord] with the updated values or
  /// a [StateFailure], if the measurement cannot finish,
  /// because it was already finished.
  Either<Failure, TimeTrackRecord> finish() {
    if (state == TimeTrackRecordState.finished) {
      return const Left(StateFailure('Can\'t finish, already finished'));
    }

    late final List<TimeTrackFragment> fragments;
    if (state == TimeTrackRecordState.paused) {
      fragments = [...this.fragments];
    } else {
      final activeFragment =
          this.fragments.singleWhere((item) => item.endDate.isNone());

      fragments = [
        ...this.fragments.where((item) => item != activeFragment),
        (activeFragment.finish() as Right).value
      ];
    }

    return Right(
      copyWith(fragments: fragments, state: TimeTrackRecordState.finished),
    );
  }

  @override
  List<Object?> get props => [
        uuid,
        fragments,
        state,
      ];

  /// Returns a copy of the current instance.
  TimeTrackRecord copyWith({
    String? uuid,
    List<TimeTrackFragment>? fragments,
    TimeTrackRecordState? state,
  }) {
    return TimeTrackRecord(
      uuid: uuid ?? this.uuid,
      fragments: fragments ?? this.fragments,
      state: state ?? this.state,
    );
  }
}
