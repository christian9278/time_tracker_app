import 'package:hive_flutter/hive_flutter.dart';

import 'constants.dart';
import 'core/models/time_track_fragment_model.dart';
import 'core/models/time_track_record_model.dart';

/// Initializes the database.
///
/// if [demoMode] is set to true, all data will be stored in a
/// separate database, which will be deleted, when the application
/// starts. [demoMode] should only be used in development.
Future<void> initDatabase({bool demoMode = true}) async {
  Hive.registerAdapter<TimeTrackRecordModel>(TimeTrackRecordModelAdapter());
  Hive.registerAdapter<TimeTrackFragmentModel>(TimeTrackFragmentModelAdapter());

  await Hive.initFlutter();

  if (demoMode) {
    await Hive.deleteBoxFromDisk(kHiveDemoDatabaseName);
    await Hive.openBox<TimeTrackRecordModel>(kHiveDemoDatabaseName);
  } else {
    await Hive.openBox<TimeTrackRecordModel>(kHiveLocalDatabaseName);
  }
}
