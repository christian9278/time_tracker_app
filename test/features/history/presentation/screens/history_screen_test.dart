import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/features/history/presentation/screens/history_screen.dart';

import '../../../../widgets_util.dart';

void main() {
  group('HistoryScreen', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      final widget = withApp(home: const Scaffold(body: HistoryScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(HistoryScreen), findsOneWidget);
    });

    testWidgets('should have a title', (tester) async {
      // Arrange
      final widget = withApp(home: const Scaffold(body: HistoryScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(AppBar), findsOneWidget);
      expect(find.text('History'.toUpperCase()), findsOneWidget);
    });
  });
}
