import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/features/track_time/presentation/widgets/stopwatch.dart';

import '../../../../widgets_util.dart';

void main() {
  group('Stopwatch', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      final widget =
          withApp(home: const Scaffold(body: Stopwatch(initial: Duration())));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(Stopwatch), findsOneWidget);
      expect(find.text('00:00:00'), findsOneWidget);
    });

    testWidgets('should update every second', (tester) async {
      // Arrange
      final widget =
          withApp(home: const Scaffold(body: Stopwatch(initial: Duration())));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();
      expect(find.text('00:00:00'), findsOneWidget);

      await tester.pumpAndSettle(const Duration(seconds: 1));
      await tester.pump();
    });
  });
}
