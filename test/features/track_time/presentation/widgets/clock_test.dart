import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/features/track_time/presentation/widgets/clock.dart';

import '../../../../widgets_util.dart';

void main() {
  group('Clock', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      final widget = withApp(home: const Scaffold(body: Clock()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(Clock), findsOneWidget);
    });

    testWidgets('should update every minute', (tester) async {
      // Arrange
      final widget = withApp(home: const Scaffold(body: Clock()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      sleep(const Duration(minutes: 1));
      await tester.pumpAndSettle(const Duration(seconds: 2));

      // Assert
      expect(find.byType(Clock), findsOneWidget);
    });
  });
}
