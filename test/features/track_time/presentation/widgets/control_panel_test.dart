import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mocktail/mocktail.dart';
import 'package:time_tracker_app/core/entities/time_track_record.dart';
import 'package:time_tracker_app/features/track_time/presentation/cubits/track_time/track_time_cubit.dart';
import 'package:time_tracker_app/features/track_time/presentation/widgets/control_panel.dart';

import '../../../../widgets_util.dart';

class MockTrackTimeCubit extends MockCubit<TrackTimeState>
    implements TrackTimeCubit {}

class TrackTimeStateFake extends Fake implements TrackTimeState {}

void main() {
  final GetIt serviceLocator = GetIt.I;
  late TrackTimeCubit mockTrackTimeCubit;

  setUpAll(() {
    registerFallbackValue(TrackTimeStateFake());
  });

  setUp(() {
    mockTrackTimeCubit = MockTrackTimeCubit();

    serviceLocator.registerFactory<TrackTimeCubit>(() => mockTrackTimeCubit);
  });

  tearDown(() {
    serviceLocator.unregister<TrackTimeCubit>();
  });

  group('ControlPanel', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state).thenReturn(
        const TrackTimeStateChange(activeTimeTrackRecord: None()),
      );

      final widget = withApp(
        home: Scaffold(
          body: BlocProvider<TrackTimeCubit>(
            create: (_) => serviceLocator<TrackTimeCubit>(),
            child: ControlPanel(
              onPause: () {},
              onResume: () {},
              onStart: () {},
              onStop: () {},
            ),
          ),
        ),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(ControlPanel), findsOneWidget);
    });

    testWidgets('should press start button and execute callback',
        (tester) async {
      // Arrange
      bool pressed = false;

      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state).thenReturn(
        const TrackTimeStateChange(activeTimeTrackRecord: None()),
      );

      final widget = withApp(
        home: Scaffold(
          body: BlocProvider<TrackTimeCubit>(
            create: (_) => serviceLocator<TrackTimeCubit>(),
            child: ControlPanel(
              onPause: () {},
              onResume: () {},
              onStart: () => pressed = true,
              onStop: () {},
            ),
          ),
        ),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      final button = find.byKey(const Key('control-panel-start-button'));
      await tester.tap(button);
      await tester.pump();

      // Assert
      expect(find.byType(ControlPanel), findsOneWidget);
      expect(pressed, isTrue);
    });

    testWidgets('should press stop button and execute callback',
        (tester) async {
      // Arrange
      bool pressed = false;

      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state).thenReturn(TrackTimeStateChange(
        activeTimeTrackRecord: Some(TimeTrackRecord.create()),
      ));

      final widget = withApp(
        home: Scaffold(
          body: BlocProvider<TrackTimeCubit>(
            create: (_) => serviceLocator<TrackTimeCubit>(),
            child: ControlPanel(
              onPause: () {},
              onResume: () {},
              onStart: () {},
              onStop: () => pressed = true,
            ),
          ),
        ),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      final button = find.byKey(const Key('control-panel-stop-button'));
      await tester.tap(button);
      await tester.pump();

      // Assert
      expect(find.byType(ControlPanel), findsOneWidget);
      expect(pressed, isTrue);
    });

    testWidgets('should press pause button and execute callback',
        (tester) async {
      // Arrange
      bool pressed = false;

      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state).thenReturn(TrackTimeStateChange(
        activeTimeTrackRecord: Some(TimeTrackRecord.create()),
      ));

      final widget = withApp(
        home: Scaffold(
          body: BlocProvider<TrackTimeCubit>(
            create: (_) => serviceLocator<TrackTimeCubit>(),
            child: ControlPanel(
              onPause: () => pressed = true,
              onResume: () {},
              onStart: () {},
              onStop: () {},
            ),
          ),
        ),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      final button = find.byKey(const Key('control-panel-pause-button'));
      await tester.tap(button);
      await tester.pump();

      // Assert
      expect(find.byType(ControlPanel), findsOneWidget);
      expect(pressed, isTrue);
    });

    testWidgets('should press resume button and execute callback',
        (tester) async {
      // Arrange
      bool pressed = false;

      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state).thenAnswer((_) {
        final newTimeTrackRecord = TimeTrackRecord.create();
        final pausedTimeTrackRecord =
            (newTimeTrackRecord.pause() as Right).value;

        return TrackTimeStateChange(
            activeTimeTrackRecord: Some(pausedTimeTrackRecord));
      });

      final widget = withApp(
        home: Scaffold(
          body: BlocProvider<TrackTimeCubit>(
            create: (_) => serviceLocator<TrackTimeCubit>(),
            child: ControlPanel(
              onPause: () {},
              onResume: () => pressed = true,
              onStart: () {},
              onStop: () {},
            ),
          ),
        ),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      final button = find.byKey(const Key('control-panel-start-button'));
      await tester.tap(button);
      await tester.pump();

      // Assert
      expect(find.byType(ControlPanel), findsOneWidget);
      expect(pressed, isTrue);
    });
  });
}
