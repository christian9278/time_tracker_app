import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/entities/time_track_record.dart';
import 'package:time_tracker_app/features/track_time/presentation/cubits/track_time/track_time_cubit.dart';

void main() {
  group('TrackTimeInitial', () {
    test('should be equatable', () {
      // Arrange
      const s1 = TrackTimeInitial();
      const s2 = TrackTimeInitial();

      // Assert
      expect(s1, s2);
    });
  });

  group('TrackTimeTrackStateChange', () {
    test('should be equatable', () {
      // Arrange
      final record = TimeTrackRecord.create();

      final s1 = TrackTimeStateChange(activeTimeTrackRecord: Some(record));
      final s2 = TrackTimeStateChange(activeTimeTrackRecord: Some(record));

      // Assert
      expect(s1, s2);
    });
  });
}
