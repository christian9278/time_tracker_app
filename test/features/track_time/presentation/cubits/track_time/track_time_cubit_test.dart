import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:rxdart/subjects.dart';
import 'package:time_tracker_app/core/entities/time_track_record.dart';
import 'package:time_tracker_app/features/track_time/domain/repositories/track_time_repository.dart';
import 'package:time_tracker_app/features/track_time/presentation/cubits/track_time/track_time_cubit.dart';

class MockTrackTimeRepository extends Mock implements TrackTimeRepository {}

class TimeTrackRecordFake extends Fake implements TimeTrackRecord {}

void main() {
  late TrackTimeRepository mockTrackTimeRepository;

  setUpAll(() {
    registerFallbackValue(TimeTrackRecordFake());
  });

  setUp(() {
    mockTrackTimeRepository = MockTrackTimeRepository();
  });

  group('TrackTimeCubit', () {
    test('should have initial state', () {
      // Arrange
      final cubit =
          TrackTimeCubit(trackTimeRepository: mockTrackTimeRepository);

      // Act
      final state = cubit.state;

      // Assert
      expect(state, isA<TrackTimeInitial>());
    });

    blocTest<TrackTimeCubit, TrackTimeState>(
      'should initialize',
      build: () {
        final subject$ =
            BehaviorSubject<Option<TimeTrackRecord>>.seeded(const None());

        when(() => mockTrackTimeRepository.active)
            .thenAnswer((_) => subject$.stream);

        return TrackTimeCubit(trackTimeRepository: mockTrackTimeRepository);
      },
      act: (cubit) async => await cubit.initialize(),
      expect: () => [isA<TrackTimeStateChange>()],
    );

    blocTest<TrackTimeCubit, TrackTimeState>(
      'should start track time',
      build: () {
        final subject$ =
            BehaviorSubject<Option<TimeTrackRecord>>.seeded(const None());

        when(() => mockTrackTimeRepository.active)
            .thenAnswer((_) => subject$.stream);

        when(() => mockTrackTimeRepository.create()).thenAnswer(
          (_) async {
            final record = TimeTrackRecord.create();

            subject$.add(Some(record));

            return Right(record);
          },
        );

        return TrackTimeCubit(trackTimeRepository: mockTrackTimeRepository);
      },
      act: (cubit) async {
        await cubit.initialize();
        await cubit.start();
      },
      expect: () => [
        isA<TrackTimeStateChange>(),
        isA<TrackTimeStateChange>(),
      ],
    );

    blocTest<TrackTimeCubit, TrackTimeState>(
      'should stop time tracking',
      build: () {
        final subject$ = BehaviorSubject<Option<TimeTrackRecord>>.seeded(
          Some(TimeTrackRecord.create()),
        );

        when(() => mockTrackTimeRepository.active)
            .thenAnswer((_) => subject$.stream);

        when(() => mockTrackTimeRepository.finish(any())).thenAnswer(
          (_) async {
            subject$.add(const None());
            return const Right(unit);
          },
        );

        return TrackTimeCubit(trackTimeRepository: mockTrackTimeRepository);
      },
      act: (cubit) async {
        await cubit.initialize();
        await cubit.stop();
      },
      expect: () => [
        isA<TrackTimeStateChange>(),
        isA<TrackTimeStateChange>(),
      ],
    );

    blocTest<TrackTimeCubit, TrackTimeState>(
      'should pause time tracking',
      build: () {
        final record = TimeTrackRecord.create();

        final subject$ = BehaviorSubject<Option<TimeTrackRecord>>.seeded(
          Some(record),
        );

        when(() => mockTrackTimeRepository.active)
            .thenAnswer((_) => subject$.stream);

        when(() => mockTrackTimeRepository.pause(any())).thenAnswer(
          (_) async {
            final pausedRecord = (record.pause() as Right).value;

            subject$.add(Some(pausedRecord));
            return Right(pausedRecord);
          },
        );

        return TrackTimeCubit(trackTimeRepository: mockTrackTimeRepository);
      },
      act: (cubit) async {
        await cubit.initialize();
        await cubit.pause();
      },
      expect: () => [
        isA<TrackTimeStateChange>(),
        isA<TrackTimeStateChange>(),
      ],
    );

    blocTest<TrackTimeCubit, TrackTimeState>(
      'should resume time tracking',
      build: () {
        final record = TimeTrackRecord.create();
        final pausedRecord = (record.pause() as Right).value;

        final subject$ = BehaviorSubject<Option<TimeTrackRecord>>.seeded(
          Some(pausedRecord),
        );

        when(() => mockTrackTimeRepository.active)
            .thenAnswer((_) => subject$.stream);

        when(() => mockTrackTimeRepository.resume(any())).thenAnswer(
          (_) async {
            final resumedRecord = (pausedRecord.resume() as Right).value;

            subject$.add(Some(resumedRecord));
            return Right(resumedRecord);
          },
        );

        return TrackTimeCubit(trackTimeRepository: mockTrackTimeRepository);
      },
      act: (cubit) async {
        await cubit.initialize();
        await cubit.resume();
      },
      expect: () => [
        isA<TrackTimeStateChange>(),
        isA<TrackTimeStateChange>(),
      ],
    );
  });
}
