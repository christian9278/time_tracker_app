import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mocktail/mocktail.dart';
import 'package:time_tracker_app/core/entities/time_track_record.dart';
import 'package:time_tracker_app/features/track_time/presentation/cubits/track_time/track_time_cubit.dart';
import 'package:time_tracker_app/features/track_time/presentation/screens/track_time_screen.dart';
import 'package:time_tracker_app/features/track_time/presentation/widgets/clock.dart';
import 'package:time_tracker_app/features/track_time/presentation/widgets/control_panel.dart';
import 'package:time_tracker_app/core/widgets/loading.dart';

import '../../../../widgets_util.dart';

class MockTrackTimeCubit extends MockCubit<TrackTimeState>
    implements TrackTimeCubit {}

class TrackTimeStateFake extends Fake implements TrackTimeState {}

void main() {
  final GetIt serviceLocator = GetIt.I;
  late TrackTimeCubit mockTrackTimeCubit;

  setUpAll(() {
    registerFallbackValue(TrackTimeStateFake());
  });

  setUp(() {
    mockTrackTimeCubit = MockTrackTimeCubit();

    serviceLocator.registerFactory<TrackTimeCubit>(() => mockTrackTimeCubit);
  });

  tearDown(() {
    serviceLocator.unregister<TrackTimeCubit>();
  });

  group('TrackTimeScreen', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state)
          .thenReturn(const TrackTimeStateChange(
        activeTimeTrackRecord: None(),
      ));

      final widget = withApp(
        home: const Scaffold(body: TrackTimeScreen()),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(TrackTimeScreen), findsOneWidget);
      expect(find.byType(Clock), findsOneWidget);
      expect(find.byType(ControlPanel), findsOneWidget);
    });

    testWidgets('should render loading widget', (tester) async {
      // Arrange
      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});

      final widget = withApp(home: const Scaffold(body: TrackTimeScreen()));
      when(() => mockTrackTimeCubit.state).thenReturn(const TrackTimeInitial());

      // Act
      await tester.pumpWidget(widget);
      await tester.pump();

      // Assert
      expect(find.byType(TrackTimeScreen), findsOneWidget);
      expect(find.byType(Clock), findsOneWidget);
      expect(find.byType(Loading), findsOneWidget);
    });

    testWidgets('should start time tracking', (tester) async {
      // Arrange
      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state)
          .thenReturn(const TrackTimeStateChange(
        activeTimeTrackRecord: None(),
      ));
      when(() => mockTrackTimeCubit.start()).thenAnswer((_) async {});

      final widget = withApp(home: const Scaffold(body: TrackTimeScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      final button = find.byKey(const Key('control-panel-start-button'));
      await tester.tap(button);
      await tester.pump();

      // Assert
      verify(() => mockTrackTimeCubit.start()).called(1);
    });

    testWidgets('should stop time tracking', (tester) async {
      // Arrange
      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state).thenReturn(TrackTimeStateChange(
        activeTimeTrackRecord: Some(TimeTrackRecord.create()),
      ));
      when(() => mockTrackTimeCubit.stop()).thenAnswer((_) async {});

      final widget = withApp(home: const Scaffold(body: TrackTimeScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      final button = find.byKey(const Key('control-panel-stop-button'));
      await tester.tap(button);
      await tester.pump();

      // Assert
      verify(() => mockTrackTimeCubit.stop()).called(1);
    });

    testWidgets('should pause time tracking', (tester) async {
      // Arrange
      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state).thenReturn(TrackTimeStateChange(
        activeTimeTrackRecord: Some(TimeTrackRecord.create()),
      ));
      when(() => mockTrackTimeCubit.pause()).thenAnswer((_) async {});

      final widget = withApp(home: const Scaffold(body: TrackTimeScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      final button = find.byKey(const Key('control-panel-pause-button'));
      await tester.tap(button);
      await tester.pump();

      // Assert
      verify(() => mockTrackTimeCubit.pause()).called(1);
    });

    testWidgets('should resume time tracking', (tester) async {
      // Arrange
      final record = TimeTrackRecord.create();

      when(() => mockTrackTimeCubit.initialize()).thenAnswer((_) async {});
      when(() => mockTrackTimeCubit.state).thenReturn(TrackTimeStateChange(
        activeTimeTrackRecord: Some((record.pause() as Right).value),
      ));
      when(() => mockTrackTimeCubit.resume()).thenAnswer((_) async {});

      final widget = withApp(home: const Scaffold(body: TrackTimeScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      final button = find.byKey(const Key('control-panel-start-button'));
      await tester.tap(button);
      await tester.pump();

      // Assert
      verify(() => mockTrackTimeCubit.resume()).called(1);
    });
  });
}
