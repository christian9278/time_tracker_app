import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:time_tracker_app/core/datasources/hive_datasource.dart';
import 'package:time_tracker_app/core/entities/time_track_record.dart';
import 'package:time_tracker_app/core/entities/time_track_record_state.dart';
import 'package:time_tracker_app/core/exceptions/record_exists_exception.dart';
import 'package:time_tracker_app/core/exceptions/record_not_found_exception.dart';
import 'package:time_tracker_app/core/failures/record_exists_failure.dart';
import 'package:time_tracker_app/core/failures/record_not_found_failure.dart';
import 'package:time_tracker_app/core/failures/state_failure.dart';
import 'package:time_tracker_app/core/models/time_track_record_model.dart';
import 'package:time_tracker_app/features/track_time/data/repositories/track_time_repository_impl.dart';

class MockTimeTrackDatasource extends Mock implements HiveDatasource {}

class TimeTrackRecordModelFake extends Fake implements TimeTrackRecordModel {}

void main() {
  late HiveDatasource mockHiveDatasource;

  setUpAll(() {
    registerFallbackValue(TimeTrackRecordModelFake());
  });

  setUp(() {
    mockHiveDatasource = MockTimeTrackDatasource();
  });

  group('TrackTimeRepositoryImpl', () {
    test('should return active TimeTrackRecord', () {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([
        const TimeTrackRecordModel(
          uuid: 'uuid',
          fragments: [],
          state: 0,
        ),
      ]);

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      // Assert
      expect(repository.active, emits(isA<Some<TimeTrackRecord>>()));
    });

    test('should return nothing if no TimeTrackRecord is active', () {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([
        const TimeTrackRecordModel(
          uuid: 'uuid',
          fragments: [],
          state: 2,
        ),
      ]);

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      // Assert
      expect(repository.active, emits(isA<None>()));
    });

    test('should create a TimeTrackRecord', () async {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.add(any())).thenAnswer((_) async {});

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final record = await repository.create();

      // Assert
      expect(record, isA<Right>());
      expect((record as Right).value, isA<TimeTrackRecord>());
      expect((record as Right).value.state, TimeTrackRecordState.tracking);
      expect(repository.active, emits(isA<Some<TimeTrackRecord>>()));
      verify(() => mockHiveDatasource.add(any())).called(1);
    });

    test(
        '''should create a TimeTrackRecord and return failure if an active record already exists''',
        () async {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([
        const TimeTrackRecordModel(
          uuid: 'uuid',
          fragments: [],
          state: 0,
        ),
      ]);
      when(() => mockHiveDatasource.add(any())).thenAnswer((_) async {});

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final record = await repository.create();

      // Assert
      expect(record, isA<Left>());
      expect((record as Left).value, isA<StateFailure>());
      verifyNever(() => mockHiveDatasource.add(any()));
    });

    test('should finish TimeTrackRecord', () async {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.put(any())).thenAnswer((_) async => {});

      final record = TimeTrackRecord.create();

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final finishedRecord = await repository.finish(record);

      // Assert
      expect(finishedRecord, isA<Right>());
      expect((finishedRecord as Right).value, isA<Unit>());
      expect(repository.active, emits(isA<None>()));
      verify(() => mockHiveDatasource.put(any())).called(1);
    });

    test(
        '''should finish TimeTrackRecord and return failure if record is not active''',
        () async {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.put(any())).thenAnswer((_) async => {});

      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final newFinishedRecord = await repository.finish(finishedRecord);

      // Assert
      expect(newFinishedRecord, isA<Left>());
      expect((newFinishedRecord as Left).value, isA<StateFailure>());
      verifyNever(() => mockHiveDatasource.put(any()));
    });

    test('should pause TimeTrackRecord', () async {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.put(any())).thenAnswer((_) async => {});

      final record = TimeTrackRecord.create();

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final pausedRecord = await repository.pause(record);

      // Assert
      expect(pausedRecord, isA<Right>());
      expect((pausedRecord as Right).value, isA<TimeTrackRecord>());
      expect((pausedRecord as Right).value.state, TimeTrackRecordState.paused);
      expect(repository.active, emits(isA<Some<TimeTrackRecord>>()));
      verify(() => mockHiveDatasource.put(any())).called(1);
    });

    test('should resume a paused TimeTrackRecord', () async {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.put(any())).thenAnswer((_) async => {});

      final record = TimeTrackRecord.create();
      final pausedRecord = (record.pause() as Right).value;

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final resumedRecord = await repository.resume(pausedRecord);

      // Assert
      expect(resumedRecord, isA<Right>());
      expect((resumedRecord as Right).value, isA<TimeTrackRecord>());
      expect(
          (resumedRecord as Right).value.state, TimeTrackRecordState.tracking);
      expect(repository.active, emits(isA<Some<TimeTrackRecord>>()));
      verify(() => mockHiveDatasource.put(any())).called(1);
    });

    test('should dispose', () async {
      // Arrange
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.dispose()).thenAnswer((_) async {});

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      await repository.dispose();

      // Assert
      verify(() => mockHiveDatasource.dispose()).called(1);
    });

    test('should return RecordExistsFailure while creating a new record',
        () async {
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.add(any())).thenThrow(
        const RecordExistsException('Exception'),
      );

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final record = await repository.create();

      // Assert
      expect(record, isA<Left>());
      expect((record as Left).value, isA<RecordExistsFailure>());
      verify(() => mockHiveDatasource.add(any())).called(1);
    });

    test(
        'should return RecordNotFoundFailure while finishing an existing record',
        () async {
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.put(any())).thenThrow(
        const RecordNotFoundException('Exception'),
      );

      final record = TimeTrackRecord.create();

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final finishedRecord = await repository.finish(record);

      // Assert
      expect(finishedRecord, isA<Left>());
      expect((finishedRecord as Left).value, isA<RecordNotFoundFailure>());
      verify(() => mockHiveDatasource.put(any())).called(1);
    });

    test('Should return StateFailure while finishing a finished record',
        () async {
      when(() => mockHiveDatasource.values).thenReturn([]);

      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final result = await repository.finish(finishedRecord);

      // Assert
      expect(result, isA<Left>());
      expect((result as Left).value, isA<StateFailure>());
      verifyNever(() => mockHiveDatasource.put(any()));
    });

    test('should return RecordNotFoundFailure while pausing an existing record',
        () async {
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.put(any())).thenThrow(
        const RecordNotFoundException('Exception'),
      );

      final record = TimeTrackRecord.create();

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final finishedRecord = await repository.pause(record);

      // Assert
      expect(finishedRecord, isA<Left>());
      expect((finishedRecord as Left).value, isA<RecordNotFoundFailure>());
      verify(() => mockHiveDatasource.put(any())).called(1);
    });

    test('Should return StateFailure while pausing a paused record', () async {
      when(() => mockHiveDatasource.values).thenReturn([]);

      final record = TimeTrackRecord.create();
      final pausedRecord = (record.pause() as Right).value;

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final result = await repository.pause(pausedRecord);

      // Assert
      expect(result, isA<Left>());
      expect((result as Left).value, isA<StateFailure>());
      verifyNever(() => mockHiveDatasource.put(any()));
    });

    test('Should return StateFailure while pausing a finished record',
        () async {
      when(() => mockHiveDatasource.values).thenReturn([]);

      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final result = await repository.pause(finishedRecord);

      // Assert
      expect(result, isA<Left>());
      expect((result as Left).value, isA<StateFailure>());
      verifyNever(() => mockHiveDatasource.put(any()));
    });

    test('should return RecordNotFoundFailure while resuming a paused record',
        () async {
      when(() => mockHiveDatasource.values).thenReturn([]);
      when(() => mockHiveDatasource.put(any())).thenThrow(
        const RecordNotFoundException('Exception'),
      );

      final record = TimeTrackRecord.create();
      final pausedRecord = (record.pause() as Right).value;

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final finishedRecord = await repository.resume(pausedRecord);

      // Assert
      expect(finishedRecord, isA<Left>());
      expect((finishedRecord as Left).value, isA<RecordNotFoundFailure>());
      verify(() => mockHiveDatasource.put(any())).called(1);
    });

    test('Should return StateFailure while resuming a finished record',
        () async {
      when(() => mockHiveDatasource.values).thenReturn([]);

      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;

      // Act
      final repository = TrackTimeRepositoryImpl(datasource: mockHiveDatasource)
        ..initialize();

      final result = await repository.resume(finishedRecord);

      // Assert
      expect(result, isA<Left>());
      expect((result as Left).value, isA<StateFailure>());
      verifyNever(() => mockHiveDatasource.put(any()));
    });
  });
}
