import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/features/settings/presentation/screens/settings_screen.dart';

import '../../../../widgets_util.dart';

void main() {
  group('SettingsScreen', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      final widget = withApp(home: const Scaffold(body: SettingsScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(SettingsScreen), findsOneWidget);
    });

    testWidgets('should have a title', (tester) async {
      // Arrange
      final widget = withApp(home: const Scaffold(body: SettingsScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(AppBar), findsOneWidget);
      expect(find.text('Settings'.toUpperCase()), findsOneWidget);
    });
  });
}
