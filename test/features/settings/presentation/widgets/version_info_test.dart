import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:time_tracker_app/features/settings/presentation/widgets/version_info.dart';

import '../../../../widgets_util.dart';

class MockPackageInfo extends Mock implements PackageInfo {}

void main() {
  late PackageInfo mockPackageInfo;

  setUp(() {
    mockPackageInfo = MockPackageInfo();
  });

  group('VersionInfo', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      when(() => mockPackageInfo.version).thenReturn('0.0.0');
      when(() => mockPackageInfo.buildNumber).thenReturn('0');

      final widget = withApp(
        home: Scaffold(body: VersionInfo(packageInfo: mockPackageInfo)),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(VersionInfo), findsOneWidget);
    });

    testWidgets(
        'should return v0.0.0+0 when version and buildNumber is not set',
        (tester) async {
      // Arrange
      when(() => mockPackageInfo.version).thenReturn('');
      when(() => mockPackageInfo.buildNumber).thenReturn('');

      final widget = withApp(
        home: Scaffold(body: VersionInfo(packageInfo: mockPackageInfo)),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(VersionInfo), findsOneWidget);
      expect(find.text('v0.0.0+0'), findsOneWidget);
    });

    testWidgets('should return version number', (tester) async {
      // Arrange
      when(() => mockPackageInfo.version).thenReturn('1.2.3');
      when(() => mockPackageInfo.buildNumber).thenReturn('4');

      final widget = withApp(
        home: Scaffold(body: VersionInfo(packageInfo: mockPackageInfo)),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(VersionInfo), findsOneWidget);
      expect(find.text('v1.2.3+4'), findsOneWidget);
    });
  });
}
