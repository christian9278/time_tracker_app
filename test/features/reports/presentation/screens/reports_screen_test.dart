import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/features/reports/presentation/screens/reports_screen.dart';

import '../../../../widgets_util.dart';

void main() {
  group('ReportsScreen', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      final widget = withApp(home: const Scaffold(body: ReportsScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(ReportsScreen), findsOneWidget);
    });

    testWidgets('should have a title', (tester) async {
      // Arrange
      final widget = withApp(home: const Scaffold(body: ReportsScreen()));

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(AppBar), findsOneWidget);
      expect(find.text('Reports'.toUpperCase()), findsOneWidget);
    });
  });
}
