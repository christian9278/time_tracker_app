import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/entities/environment.dart';

void main() {
  group('Environment', () {
    test('should be equatable', () {
      // Arrange
      const e1 = Environment();
      const e2 = Environment();

      // Assert
      expect(e1, e2);
    });

    test(
        '''should provide default values if no environment variables were provided''',
        () {
      // Arrange
      final environment = Environment.fromEnvironment();

      // Assert
      expect(environment.demoMode, isFalse);
      expect(environment.devMode, isFalse);
      expect(environment.logLevel, 800);
    });
  });
}
