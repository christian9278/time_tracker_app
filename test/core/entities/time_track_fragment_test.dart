import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/entities/time_track_fragment.dart';
import 'package:time_tracker_app/core/failures/state_failure.dart';

void main() {
  group('TimeTrackFragment', () {
    test('should create new instance', () {
      // Act
      final fragment = TimeTrackFragment.create();

      // Assert
      expect(fragment, isA<TimeTrackFragment>());
      expect(fragment.startDate, isA<DateTime>());
      expect(fragment.endDate, isA<None>());
      expect(fragment.duration, isA<None>());
    });

    test('should return true if TimeTrackFragment is active', () {
      // Arrange
      final fragment = TimeTrackFragment.create();

      // Act
      final isActive = fragment.isActive;

      // Assert
      expect(isActive, isTrue);
    });

    test('should return false if TimeTrackFragment is not active', () {
      // Arrange
      final fragment = TimeTrackFragment.create();

      // Act
      final finishedFragment = (fragment.finish() as Right).value;
      final isActive = finishedFragment.isActive;

      // Assert
      expect(isActive, isFalse);
    });

    test('should finish TimeTrackFragment', () {
      // Arrange
      final fragment = TimeTrackFragment.create();
      expect(fragment.isActive, isTrue);

      // Act
      final finishedFragment = (fragment.finish() as Right).value;

      // Assert
      expect(finishedFragment.isActive, isFalse);
      expect(finishedFragment.endDate, isA<Some>());
    });

    test(
        '''should finish TimeTrackFragment and return Failure if fragment is already finished''',
        () {
      // Arrange
      final fragment = TimeTrackFragment.create();
      final finishedFragment = (fragment.finish() as Right).value;
      expect(finishedFragment.isActive, isFalse);

      // Act
      final result = (finishedFragment.finish() as Left).value;

      // Assert
      expect(result, isA<StateFailure>());
    });

    test('should be equatable', () {
      // Arrange
      final f1 = TimeTrackFragment.create();
      final f2 = f1.copyWith();

      final f3 = (f1.finish() as Right).value;
      final f4 = f3.copyWith();

      // Assert
      expect(f1, f2);
      expect(f3, f4);
    });
  });
}
