import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/entities/time_track_fragment.dart';
import 'package:time_tracker_app/core/entities/time_track_record.dart';
import 'package:time_tracker_app/core/entities/time_track_record_state.dart';
import 'package:time_tracker_app/core/failures/state_failure.dart';

void main() {
  group('TimeTrackRecord', () {
    test('should create new instance', () {
      // Act
      final record = TimeTrackRecord.create();

      // Assert
      expect(record, isA<TimeTrackRecord>());
      expect(record.uuid, isNotEmpty);
      expect(record.fragments, isNotEmpty);
      expect(record.fragments.length, 1);
      expect(record.state, TimeTrackRecordState.tracking);
    });

    test('should return true if TimeTrackRecord is active', () {
      // Arrange
      final record = TimeTrackRecord.create();
      expect(record.state, TimeTrackRecordState.tracking);

      // Act
      final isActive = record.isActive;

      // Assert
      expect(isActive, isTrue);
    });

    test('should return false if TimeTrackRecord is not active', () {
      // Arrange
      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;
      expect(finishedRecord.state, TimeTrackRecordState.finished);

      // Act
      final isActive = finishedRecord.isActive;

      // Assert
      expect(isActive, isFalse);
    });

    test('should return true if TimeTrackRecord is paused', () {
      // Arrange
      final record = TimeTrackRecord.create();
      final pausedRecord = (record.pause() as Right).value;
      expect(pausedRecord.state, TimeTrackRecordState.paused);

      // Act
      final isPaused = pausedRecord.isPaused;

      // Assert
      expect(isPaused, isTrue);
    });

    test('should return false if TimeTrackRecord is not paused', () {
      // Arrange
      final record = TimeTrackRecord.create();
      expect(record.state, TimeTrackRecordState.tracking);

      // Act
      final isPaused = record.isPaused;

      // Assert
      expect(isPaused, isFalse);
    });

    test('should return the total duration for an active TimeTrackRecord', () {
      // Arrange
      final activeFragment = TimeTrackFragment(
        startDate: DateTime.now().subtract(const Duration(minutes: 1)),
        endDate: const None(),
        duration: const None(),
      );

      final record = TimeTrackRecord(
        uuid: 'uuid',
        fragments: [activeFragment],
        state: TimeTrackRecordState.tracking,
      );

      // Act
      final totalDuration = record.totalDuration;

      // Assert
      expect(totalDuration.inMinutes, 1);
    });

    test('should return the total duration for finished TimeTrackRecord', () {
      // Arrange
      final fragment1 = TimeTrackFragment(
        startDate: DateTime.now().subtract(const Duration(minutes: 1)),
        endDate: Some(DateTime.now()),
        duration: const Some(Duration(minutes: 1)),
      );

      final fragment2 = TimeTrackFragment(
        startDate: DateTime.now().subtract(const Duration(minutes: 5)),
        endDate: Some(DateTime.now().subtract(const Duration(minutes: 4))),
        duration: const Some(Duration(minutes: 1)),
      );

      final record = TimeTrackRecord(
        uuid: 'uuid',
        fragments: [fragment1, fragment2],
        state: TimeTrackRecordState.finished,
      );

      // Act
      final totalDuration = record.totalDuration;

      // Assert
      expect(totalDuration.inMinutes, 2);
    });

    test('should pause a TimeTrackRecord', () {
      // Arrange
      final record = TimeTrackRecord.create();

      // Act
      final pausedRecord = (record.pause() as Right).value;

      // Assert
      expect(pausedRecord.state, TimeTrackRecordState.paused);
      expect(pausedRecord.isActive, isTrue);
      expect(pausedRecord.isPaused, isTrue);
      expect(
        pausedRecord.fragments.every((fragment) => !fragment.isActive),
        isTrue,
      );
    });

    test(
        '''should pause a TimeTrackRecord and return a Failure if record is already paused''',
        () {
      // Arrange
      final record = TimeTrackRecord.create();
      final pausedRecord = (record.pause() as Right).value;

      // Act
      final failedRecord = (pausedRecord.pause() as Left).value;

      // Assert
      expect(failedRecord, isA<StateFailure>());
    });

    test(
        '''should pause a TimeTrackRecord and return a Failure if record is already finished''',
        () {
      // Arrange
      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;

      // Act
      final failedRecord = (finishedRecord.finish() as Left).value;

      // Assert
      expect(failedRecord, isA<StateFailure>());
    });

    test('should resume a paused TimeTrackRecord', () {
      // Arrange
      final record = TimeTrackRecord.create();
      final pausedRecord = (record.pause() as Right).value;
      expect(pausedRecord.state, TimeTrackRecordState.paused);

      // Act
      final resumedRecord = (pausedRecord.resume() as Right).value;

      // Assert
      expect(resumedRecord.state, TimeTrackRecordState.tracking);
      expect(resumedRecord.fragments.last.isActive, isTrue);
    });

    test(
        '''should resumeTimeTrackRecord and return a Failure if record is active''',
        () {
      // Arrange
      final record = TimeTrackRecord.create();
      expect(record.state, TimeTrackRecordState.tracking);

      // Act
      final failedRecord = (record.resume() as Left).value;

      // Assert
      expect(failedRecord, isA<StateFailure>());
    });

    test(
        '''should resume a TimeTrackRecord and return a Failure if record is finished''',
        () {
      // Arrange
      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;
      expect(finishedRecord.state, TimeTrackRecordState.finished);

      // Act
      final failedRecord = (finishedRecord.resume() as Left).value;

      // Assert
      expect(failedRecord, isA<StateFailure>());
    });

    test('should finish an active TimeTrackRecord', () {
      // Arrange
      final record = TimeTrackRecord.create();
      expect(record.state, TimeTrackRecordState.tracking);

      // Act
      final finishedRecord = (record.finish() as Right).value;

      // Assert
      expect(finishedRecord.state, TimeTrackRecordState.finished);
      expect(
        finishedRecord.fragments.every((fragment) => !fragment.isActive),
        isTrue,
      );
    });

    test('should finish a paused TimeTrackRecord', () {
      // Arrange
      final record = TimeTrackRecord.create();
      final pausedRecord = (record.pause() as Right).value;
      expect(pausedRecord.state, TimeTrackRecordState.paused);

      // Act
      final finishedRecord = (pausedRecord.finish() as Right).value;

      // Assert
      expect(finishedRecord.state, TimeTrackRecordState.finished);
      expect(
        finishedRecord.fragments.every((fragment) => !fragment.isActive),
        isTrue,
      );
    });

    test(
        '''should finish a TimeTrackRecord and return a Failure if record is already finished''',
        () {
      // Arrange
      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;
      expect(finishedRecord.state, TimeTrackRecordState.finished);

      // Act
      final failedRecord = (finishedRecord.finish() as Left).value;

      // Assert
      expect(failedRecord, isA<StateFailure>());
    });

    test('should return a start date', () {
      // Arrange
      final firstDateTime = DateTime.now();

      final record = TimeTrackRecord.create();
      expect(record.state, TimeTrackRecordState.tracking);

      final lastDateTime = DateTime.now();

      // Assert
      expect(record.startDate, isA<DateTime>());
      expect(record.startDate.isAfter(firstDateTime), isTrue);
      expect(record.startDate.isBefore(lastDateTime), isTrue);
    });

    test('should not return an end date if record is not finished', () {
      // Arrange
      final record = TimeTrackRecord.create();
      expect(record.state, TimeTrackRecordState.tracking);

      // Act
      final endDate = (record.endDate as None);

      // Assert
      expect(endDate, isA<None>());
    });

    test('should return an end date if record is finished', () {
      // Arrange
      final record = TimeTrackRecord.create();
      final finishedRecord = (record.finish() as Right).value;

      // Act
      final endDate = (finishedRecord.endDate as Some);

      // Assert
      expect(endDate, isA<Some>());
    });

    test('should be equatable', () {
      // Arrange
      final r1 = TimeTrackRecord.create();
      final r2 = r1.copyWith();

      // Assert
      expect(r1, r2);
    });
  });
}
