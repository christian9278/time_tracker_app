import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/cubits/environment/environment_cubit.dart';
import 'package:time_tracker_app/core/entities/environment.dart';

import '../../../test_util.dart';

void main() {
  group('EnvironmentInitial', () {
    test('should be equatable', () {
      // Arrange
      final e1 = EnvironmentInitial(
        environment: const Environment(),
        packageInfo: PackageInfoFake(),
        platform: PlatformFake(),
      );

      final e2 = EnvironmentInitial(
        environment: const Environment(),
        packageInfo: PackageInfoFake(),
        platform: PlatformFake(),
      );

      // Assert
      expect(e1, e2);
    });
  });
}
