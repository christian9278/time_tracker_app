import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/cubits/environment/environment_cubit.dart';

import '../../../test_util.dart';

void main() {
  group('EnvironmentCubit', () {
    test('should have initial state', () {
      // Arrange
      final cubit = EnvironmentCubit(
        environment: EnvironmentFake(),
        packageInfo: PackageInfoFake(),
        platform: PlatformFake(),
      );

      // Act
      final state = cubit.state;

      // Assert
      expect(state, isA<EnvironmentInitial>());
    });
  });
}
