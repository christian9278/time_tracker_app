import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:mocktail/mocktail.dart';
import 'package:time_tracker_app/core/datasources/hive_datasource_impl.dart';
import 'package:time_tracker_app/core/exceptions/record_exists_exception.dart';
import 'package:time_tracker_app/core/exceptions/record_not_found_exception.dart';
import 'package:time_tracker_app/core/models/time_track_record_model.dart';

const String kHiveTestDatabaseName = 'time_track_test';

class MockHiveInterface extends Mock implements HiveInterface {}

class MockBox<T> extends Mock implements Box<T> {}

class TimeTrackRecordModelFake extends Fake implements TimeTrackRecordModel {}

void main() {
  late Box<TimeTrackRecordModel> mockBox;
  late HiveInterface mockHiveInterface;

  setUpAll(() {
    registerFallbackValue(TimeTrackRecordModelFake());
  });

  setUp(() {
    mockBox = MockBox<TimeTrackRecordModel>();
    mockHiveInterface = MockHiveInterface();

    when(() => mockHiveInterface.box<TimeTrackRecordModel>(any()))
        .thenReturn(mockBox);
  });

  group('HiveLocalDatasourceImpl', () {
    test('should create instance', () {
      // Act
      final datasource = HiveLocalDatasourceImpl.create(
        mockHiveInterface,
        databaseName: kHiveTestDatabaseName,
      );

      // Assert
      expect(datasource, isA<HiveLocalDatasourceImpl>());
    });

    test('should return an empty list', () {
      // Arrange
      when(() => mockBox.values).thenReturn([]);

      // Act
      final datasource = HiveLocalDatasourceImpl.create(
        mockHiveInterface,
        databaseName: kHiveTestDatabaseName,
      );
      final values = datasource.values;

      // Assert
      expect(values, isEmpty);
    });

    test('should return a list of values', () {
      // Arrange
      final models = [
        const TimeTrackRecordModel(uuid: 'uuid1', fragments: [], state: 1),
        const TimeTrackRecordModel(uuid: 'uuid2', fragments: [], state: 0),
      ];

      when(() => mockBox.values).thenReturn(models);

      // Act
      final datasource = HiveLocalDatasourceImpl.create(
        mockHiveInterface,
        databaseName: kHiveTestDatabaseName,
      );
      final values = datasource.values;

      // Assert
      expect(values, isNotEmpty);
    });

    test('should add a value', () async {
      // Arrange
      when(() => mockBox.keys).thenReturn([]);
      when(() => mockBox.put(any(), any())).thenAnswer((_) async {});

      const model = TimeTrackRecordModel(
        uuid: 'uuid',
        fragments: [],
        state: 0,
      );

      // Act
      final datasource = HiveLocalDatasourceImpl.create(
        mockHiveInterface,
        databaseName: kHiveTestDatabaseName,
      );
      await datasource.add(model);

      // Assert
      verify(() => mockBox.put('uuid', model)).called(1);
    });

    test('should add a value and throw an Exception if key already exists',
        () async {
      // Arrange
      when(() => mockBox.keys).thenReturn(['uuid']);
      when(() => mockBox.put(any(), any())).thenAnswer((_) async {});

      const model = TimeTrackRecordModel(
        uuid: 'uuid',
        fragments: [],
        state: 0,
      );

      // Act
      final datasource = HiveLocalDatasourceImpl.create(
        mockHiveInterface,
        databaseName: kHiveTestDatabaseName,
      );

      // Assert
      expectLater(
        () async => await datasource.add(model),
        throwsA(isA<RecordExistsException>()),
      );
      verifyNever(() => mockBox.put('uuid', model));
    });

    test('should update a value', () async {
      // Arrange
      when(() => mockBox.keys).thenReturn(['uuid']);
      when(() => mockBox.put(any(), any())).thenAnswer((_) async {});

      const model = TimeTrackRecordModel(
        uuid: 'uuid',
        fragments: [],
        state: 0,
      );

      // Act
      final datasource = HiveLocalDatasourceImpl.create(
        mockHiveInterface,
        databaseName: kHiveTestDatabaseName,
      );
      await datasource.put(model);

      // Assert
      verify(() => mockBox.put('uuid', model)).called(1);
    });

    test('should update a value and throw an Exception if key does not exists',
        () async {
      // Arrange
      when(() => mockBox.keys).thenReturn([]);
      when(() => mockBox.put(any(), any())).thenAnswer((_) async {});

      const model = TimeTrackRecordModel(
        uuid: 'uuid',
        fragments: [],
        state: 0,
      );

      // Act
      final datasource = HiveLocalDatasourceImpl.create(
        mockHiveInterface,
        databaseName: kHiveTestDatabaseName,
      );

      // Assert
      expectLater(
        () async => await datasource.put(model),
        throwsA(isA<RecordNotFoundException>()),
      );
      verifyNever(() => mockBox.put('uuid', model));
    });

    test('should dispose', () async {
      // Arrange
      when(() => mockBox.compact()).thenAnswer((_) async => {});
      when(() => mockBox.close()).thenAnswer((_) async => {});

      // Act
      final datasource = HiveLocalDatasourceImpl.create(
        mockHiveInterface,
        databaseName: kHiveTestDatabaseName,
      );
      await datasource.dispose();

      // Assert
      verify(() => mockBox.compact()).called(1);
      verify(() => mockBox.close()).called(1);
    });
  });
}
