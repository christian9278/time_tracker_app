import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/widgets/loading.dart';

import '../../widgets_util.dart';

void main() {
  group('Loading', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      final widget = withApp(
        home: const Scaffold(body: Loading()),
      );

      // Act
      await tester.pumpWidget(widget);

      // Assert
      expect(find.byType(Loading), findsOneWidget);
      expect(find.text('Loading'), findsOneWidget);
    });
  });
}
