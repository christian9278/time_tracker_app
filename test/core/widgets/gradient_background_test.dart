import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/widgets/gradient_background.dart';

import '../../widgets_util.dart';

void main() {
  group('GradientBackground', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      final widget = withApp(
        home: GradientBackground(child: Container()),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(GradientBackground), findsOneWidget);
    });
  });
}
