import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:platform/platform.dart';
import 'package:time_tracker_app/core/cubits/environment/environment_cubit.dart';
import 'package:time_tracker_app/core/entities/environment.dart';
import 'package:time_tracker_app/core/widgets/gradient_background_scaffold.dart';
import 'package:time_tracker_app/core/widgets/gradient_background.dart';

import '../../test_util.dart';
import '../../widgets_util.dart';

class MockEnvironment extends Mock implements Environment {}

void main() {
  late Environment mockEnvironment;
  late PackageInfo mockPackageInfo;
  late Platform mockPlatform;

  setUp(() {
    mockEnvironment = MockEnvironment();
    mockPackageInfo = PackageInfoFake();
    mockPlatform = PlatformFake();
  });

  group('GradientBackgroundScaffold', () {
    testWidgets('should render properly', (tester) async {
      // Arrange
      when(() => mockEnvironment.demoMode).thenReturn(false);
      when(() => mockEnvironment.devMode).thenReturn(false);

      final widget = BlocProvider<EnvironmentCubit>(
        create: (context) => EnvironmentCubit(
          environment: mockEnvironment,
          packageInfo: mockPackageInfo,
          platform: mockPlatform,
        ),
        child: withApp(
          home: GradientBackgroundScaffold(body: Container()),
        ),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      expect(find.byType(GradientBackground), findsOneWidget);
    });

    testWidgets('should render demo banner when app runs in demo mode',
        (tester) async {
      // Arrange
      when(() => mockEnvironment.demoMode).thenReturn(true);
      when(() => mockEnvironment.devMode).thenReturn(false);

      final widget = BlocProvider<EnvironmentCubit>(
        create: (context) => EnvironmentCubit(
          environment: mockEnvironment,
          packageInfo: mockPackageInfo,
          platform: mockPlatform,
        ),
        child: withApp(
          home: GradientBackgroundScaffold(body: Container()),
        ),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      final demoBanner = find
          .byType(Banner)
          .evaluate()
          .map<Banner>((element) => element.widget as Banner)
          .firstWhere((banner) => banner.message == 'DEMO');

      expect(find.byType(GradientBackground), findsOneWidget);
      expect(demoBanner, isA<Banner>());
    });

    testWidgets('should render dev banner when app runs in dev mode',
        (tester) async {
      // Arrange
      when(() => mockEnvironment.demoMode).thenReturn(false);
      when(() => mockEnvironment.devMode).thenReturn(true);

      final widget = BlocProvider<EnvironmentCubit>(
        create: (context) => EnvironmentCubit(
          environment: mockEnvironment,
          packageInfo: mockPackageInfo,
          platform: mockPlatform,
        ),
        child: withApp(
          home: GradientBackgroundScaffold(body: Container()),
        ),
      );

      // Act
      await tester.pumpWidget(widget);
      await tester.pumpAndSettle();

      // Assert
      final demoBanner = find
          .byType(Banner)
          .evaluate()
          .map<Banner>((element) => element.widget as Banner)
          .firstWhere((banner) => banner.message == 'DEV');

      expect(find.byType(GradientBackground), findsOneWidget);
      expect(demoBanner, isA<Banner>());
    });
  });
}
