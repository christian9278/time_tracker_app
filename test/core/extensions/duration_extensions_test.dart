import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/extensions/duration_extensions.dart';

void main() {
  group('DurationExtensions', () {
    test('should display Duration in format hh:mm', () {
      // Arrange
      const duration = Duration(milliseconds: 0);

      // Act
      final str = duration.toHoursMinutes();

      // Assert
      expect(str, '00:00');
    });

    test('should display Duration in format hhh:mm', () {
      // Arrange
      const duration = Duration(hours: 100);

      // Act
      final str = duration.toHoursMinutes();

      // Assert
      expect(str, '100:00');
    });

    test('should display Duration in format hh:mm:ss', () {
      // Arrange
      const duration = Duration(milliseconds: 0);

      // Act
      final str = duration.toHoursMinutesSeconds();

      // Assert
      expect(str, '00:00:00');
    });

    test('should display Duration in format hhh:mm:ss', () {
      // Arrange
      const duration = Duration(hours: 100);

      // Act
      final str = duration.toHoursMinutesSeconds();

      // Assert
      expect(str, '100:00:00');
    });
  });
}
