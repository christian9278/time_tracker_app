import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/extensions/date_time_extensions.dart';

void main() {
  group('DateTimeExtensions', () {
    test('should return the number of the week for Jan, 1st 2001', () {
      // Arrange
      final d = DateTime(2001, 1, 1);

      // Act
      final week = d.week;

      // Assert
      expect(week, 1);
    });

    test('should return the number of the week for Dec, 31th 2000', () {
      // Arrange
      final d = DateTime(2000, 12, 31);

      // Act
      final week = d.week;

      // Assert
      expect(week, 52);
    });

    test('should return the number of the week for Jan, 1st 2023', () {
      // Arrange
      final d = DateTime(2023, 1, 1);

      // Act
      final week = d.week;

      // Assert
      expect(week, 52); // Yes, thats right, Jan 1st 2023 is in week 52
    });

    test('should return the number of the week for Jan, 2st 2023', () {
      // Arrange
      final d = DateTime(2023, 1, 2);

      // Act
      final week = d.week;

      // Assert
      expect(week, 1);
    });

    test('should return the number of the week for Dec, 31th 2023', () {
      // Arrange
      final d = DateTime(2023, 12, 31);

      // Act
      final week = d.week;

      // Assert
      expect(week, 52);
    });

    test('should return the number of the week for Jan, 1st 2001', () {
      // Arrange
      final d = DateTime(2023, 7, 17);

      // Act
      final week = d.week;

      // Assert
      expect(week, 29);
    });

    test('should return true if current date is today', () {
      // Arrange
      final d = DateTime.now();

      // Act
      final isToday = d.isToday;

      // Assert
      expect(isToday, isTrue);
    });

    test('should return false if current date is not today', () {
      // Arrange
      final d = DateTime.now().subtract(const Duration(days: 1));

      // Act
      final isToday = d.isToday;

      // Assert
      expect(isToday, isFalse);
    });

    test('should return true if current date was yesterday', () {
      // Arrange
      final d = DateTime.now().subtract(const Duration(days: 1));

      // Act
      final isYesterday = d.isYesterday;

      // Assert
      expect(isYesterday, isTrue);
    });

    test('should return false if current date was not yesterday', () {
      // Arrange
      final d = DateTime.now();

      // Act
      final isYesterday = d.isYesterday;

      // Assert
      expect(isYesterday, isFalse);
    });

    test('should return true if current date is in current week', () {
      // Arrange
      final d = DateTime.now();

      // Act
      final isInWeek = d.isInWeek;

      // Assert
      expect(isInWeek, isTrue);
    });

    test('should return false if current date is not in current week', () {
      // Arrange
      final d = DateTime.now().subtract(const Duration(days: 8));

      // Act
      final isInWeek = d.isInWeek;

      // Assert
      expect(isInWeek, isFalse);
    });

    test('should return true if current date is in current month', () {
      // Arrange
      final d = DateTime.now();

      // Act
      final isInMonth = d.isInMonth;

      // Assert
      expect(isInMonth, isTrue);
    });

    test('should return false if current date is not in current month', () {
      // Arrange
      final d = DateTime.now().subtract(const Duration(days: 35));

      // Act
      final isInMonth = d.isInMonth;

      // Assert
      expect(isInMonth, isFalse);
    });

    test('should return true if current date is in current year', () {
      // Arrange
      final d = DateTime.now();

      // Act
      final isInYear = d.isInYear;

      // Assert
      expect(isInYear, isTrue);
    });

    test('should return false if current date is not in current year', () {
      // Arrange
      final d = DateTime.now().subtract(const Duration(days: 370));

      // Act
      final isInYear = d.isInYear;

      // Assert
      expect(isInYear, isFalse);
    });

    test('should return true, if current date is a leap year', () {
      // Arrange
      final d = DateTime(2000);

      // Act
      final isLeapYear = d.isLeapYear;

      // Assert
      expect(isLeapYear, isTrue);
    });

    test('should return false, if current date is not a leap year', () {
      // Arrange
      final d = DateTime(2023);

      // Act
      final isLeapYear = d.isLeapYear;

      // Assert
      expect(isLeapYear, isFalse);
    });
  });
}
