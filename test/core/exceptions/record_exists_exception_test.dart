import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/exceptions/record_exists_exception.dart';

void main() {
  group('RecordExistsException', () {
    test('should return a message', () {
      // Arrange
      const e = RecordExistsException('Test');

      // Assert
      expect(e, isA<Exception>());
      expect(e.message, 'Test');
    });
  });
}
