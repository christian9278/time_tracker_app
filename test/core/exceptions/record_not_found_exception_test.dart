import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/exceptions/record_not_found_exception.dart';

void main() {
  group('RecordNotFoundException', () {
    test('should return a message', () {
      // Arrange
      const e = RecordNotFoundException('Test');

      // Assert
      expect(e, isA<Exception>());
      expect(e.message, 'Test');
    });
  });
}
