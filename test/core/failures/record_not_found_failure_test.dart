import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/failures/record_not_found_failure.dart';

import '../../test_util.dart';

void main() {
  group('RecordNotFoundFailure', () {
    test('should be equatable', () {
      // Arrange
      final e = GenericTestException();

      final f1 = RecordNotFoundFailure('Failure', exception: e);
      final f2 = RecordNotFoundFailure('Failure', exception: e);

      // Assert
      expect(f1, f2);
    });
  });
}
