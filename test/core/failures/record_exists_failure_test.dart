import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/failures/record_exists_failure.dart';

import '../../test_util.dart';

void main() {
  group('RecordExistsFailure', () {
    test('should be equatable', () {
      // Arrange
      final e = GenericTestException();

      final f1 = RecordExistsFailure('Failure', exception: e);
      final f2 = RecordExistsFailure('Failure', exception: e);

      // Assert
      expect(f1, f2);
    });
  });
}
