import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/failures/state_failure.dart';

void main() {
  group('StateFailure', () {
    test('should return a message', () {
      // Arrange
      const f = StateFailure('Failure');

      // Assert
      expect(f.message, 'Failure');
    });

    test('should be equatable', () {
      // Arrange
      const f1 = StateFailure('Failure');
      const f2 = StateFailure('Failure');

      // Assert
      expect(f1, f2);
    });
  });
}
