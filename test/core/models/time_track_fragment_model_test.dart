import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/entities/time_track_fragment.dart';
import 'package:time_tracker_app/core/models/time_track_fragment_model.dart';

void main() {
  group('TimeTrackFragmentModel', () {
    test('should be equatable', () {
      // Arrange
      final startDate = DateTime.now().subtract(const Duration(minutes: 1));
      final endDate = DateTime.now();
      final duration = endDate.difference(startDate);

      final m1 = TimeTrackFragmentModel(
        startDate: startDate,
        endDate: endDate,
        duration: duration.inMilliseconds,
      );

      final m2 = TimeTrackFragmentModel(
        startDate: startDate,
        endDate: endDate,
        duration: duration.inMilliseconds,
      );

      // Assert
      expect(m1, m2);
    });

    test('should map an active TimeTrackFragment to a TimeTrackFragmentModel',
        () {
      // Arrange
      final entity = TimeTrackFragment.create();

      // Act
      final model = TimeTrackFragmentModel.fromEntity(entity);

      // Assert
      expect(model, isA<TimeTrackFragmentModel>());
      expect(model.startDate, entity.startDate);
      expect(model.endDate, isNull);
      expect(model.duration, isNull);
    });

    test('should map a finished TimeTrackFragment to a TimeTrackFragmentModel',
        () {
      // Arrange
      final entity = TimeTrackFragment.create();
      final finishedEntity = (entity.finish() as Right).value;

      // Act
      final model = TimeTrackFragmentModel.fromEntity(finishedEntity);

      // Assert
      expect(model, isA<TimeTrackFragmentModel>());
      expect(model.startDate, finishedEntity.startDate);
      expect(model.endDate, (finishedEntity.endDate as Some).value);
      expect(model.duration,
          (finishedEntity.duration as Some).value.inMilliseconds);
    });

    test('should map an active TimeTrackFragmentModel to a TimeTrackFragment',
        () {
      // Arrange
      final model = TimeTrackFragmentModel(
        startDate: DateTime.now(),
        endDate: null,
        duration: null,
      );

      // Act
      final entity = model.toEntity();

      // Assert
      expect(entity, isA<TimeTrackFragment>());
      expect(entity.startDate, model.startDate);
      expect(entity.endDate, isA<None>());
      expect(entity.duration, isA<None>());
    });

    test('should map a finished TimeTrackFragmentModel to a TimeTrackFragment',
        () {
      // Arrange
      final model = TimeTrackFragmentModel(
        startDate: DateTime.now().subtract(const Duration(minutes: 1)),
        endDate: DateTime.now(),
        duration: const Duration(minutes: 1).inMilliseconds,
      );

      // Act
      final entity = model.toEntity();

      // Assert
      expect(entity, isA<TimeTrackFragment>());
      expect(entity.startDate, model.startDate);
      expect((entity.endDate as Some).value, model.endDate);
      expect((entity.duration as Some).value.inMilliseconds, model.duration);
    });
  });
}
