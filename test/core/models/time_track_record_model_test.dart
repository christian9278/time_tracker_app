import 'package:flutter_test/flutter_test.dart';
import 'package:time_tracker_app/core/entities/time_track_record.dart';
import 'package:time_tracker_app/core/models/time_track_record_model.dart';

void main() {
  group('TimeTrackRecordModel', () {
    test('should be equatable', () {
      // Arrange
      const m1 = TimeTrackRecordModel(
        uuid: 'uuid',
        fragments: [],
        state: 0,
      );

      const m2 = TimeTrackRecordModel(
        uuid: 'uuid',
        fragments: [],
        state: 0,
      );

      // Assert
      expect(m1, m2);
    });

    test('should map a TimeTrackRecord to a TimeTrackRecordModel', () {
      // Arrange
      final entity = TimeTrackRecord.create();

      // Act
      final model = TimeTrackRecordModel.fromEntity(entity);

      // Assert
      expect(model, isA<TimeTrackRecordModel>());
      expect(model.uuid, entity.uuid);
      expect(model.fragments.length, entity.fragments.length);
      expect(model.state, entity.state.index);
    });

    test('should map a TimeTrackRecordModel to a TimeTrackRecord', () {
      // Arrange
      const model = TimeTrackRecordModel(
        uuid: 'uuid',
        fragments: [],
        state: 0,
      );

      // Act
      final entity = model.toEntity();

      // Assert
      expect(entity, isA<TimeTrackRecord>());
      expect(entity.uuid, model.uuid);
      expect(entity.fragments.length, model.fragments.length);
      expect(entity.state.index, model.state);
    });
  });
}
