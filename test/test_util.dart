import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:mocktail/mocktail.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:platform/platform.dart';
import 'package:time_tracker_app/core/cubits/environment/environment_cubit.dart';
import 'package:time_tracker_app/core/entities/environment.dart';
import 'package:time_tracker_app/core/failures/failure.dart';

// Mocks
class MockNavigatorObserver extends Mock implements NavigatorObserver {}

// Fakes
class EnvironmentFake extends Fake implements Environment {
  @override
  bool get demoMode => false;

  @override
  bool get devMode => false;

  @override
  int get logLevel => 800;
}

class EnvironmentStateFake extends Fake implements EnvironmentState {}

class PackageInfoFake extends Fake with EquatableMixin implements PackageInfo {
  @override
  String get buildNumber => '0';

  @override
  String get version => '0.0.0';

  @override
  List<Object> get props => [buildNumber, version];
}

class PlatformFake extends Fake with EquatableMixin implements Platform {
  @override
  bool get isIOS => true;

  @override
  List<Object> get props => [isIOS];
}

class RouteFake extends Fake implements Route<dynamic> {}

// Exceptions and Failures
class GenericTestException implements Exception {
  final dynamic message;

  GenericTestException([this.message]);

  @override
  String toString() {
    Object? message = this.message;

    if (message == null) {
      return "Exception";
    }

    return "Exception: $message";
  }
}

class GenericTestFailure extends Failure {
  const GenericTestFailure(
    String message, {
    Exception? exception,
    StackTrace? stackTrace,
  }) : super(message, exception: exception, stackTrace: stackTrace);
}
