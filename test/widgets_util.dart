import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Widget withApp({
  required Widget home,
  ThemeData? theme,
  Map<String, Widget Function(BuildContext)> routes =
      const <String, WidgetBuilder>{},
  List<NavigatorObserver> navigatorObservers = const <NavigatorObserver>[],
}) {
  return MaterialApp(
    localizationsDelegates: AppLocalizations.localizationsDelegates,
    home: home,
    theme: theme,
    routes: routes,
    navigatorObservers: navigatorObservers,
  );
}
