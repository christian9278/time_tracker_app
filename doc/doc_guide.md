# Documentation Guide for Time Tracker App

### Generate Documentation

    dart doc

### Show Documentation

    open doc/api/index.html