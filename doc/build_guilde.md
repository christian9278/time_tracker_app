# Build Guide for Time Tracker App

### Generate Language Resources

    flutter gen-l10n

### Generate Resources

    dart run build_runner build --delete-conflicting-outputs

### Regenerate App Icon

    dart run flutter_launcher_icons

### Generate Splash Screen

    dart run flutter_native_splash:create

### Remove Splash Screen and restore default

    dart run flutter_native_splash:remove
