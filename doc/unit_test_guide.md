# Test Guide for Time Track App

### Run Unit Tests with Code Coverage

    flutter test --coverage

### Clean Up Code Coverage

    lcov -r coverage/lcov.info '*.g.dart' 'lib/bootstrap.dart' -o coverage/lcov.info

### Generate Code Coverage Report

    genhtml coverage/lcov.info -o coverage/html

### Show Code Coverage Report

    open coverage/html/index.html
