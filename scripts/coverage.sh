echo "Generating Code Coverage Report"

flutter test --coverage
lcov -r coverage/lcov.info '*.g.dart' 'lib/bootstrap.dart' -o coverage/lcov.info
genhtml coverage/lcov.info -o coverage/html
open coverage/html/index.html